

# IndexVehicleSearchResponseItem

Contains the relavant search response data.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **String** | The internal UID of the vehicle. | 
**dateIndexed** | **OffsetDateTime** | The timestamp the vehicle was indexed. | 
**vehicleTypeKey** | **String** | Type of indexed vehicle, e.g. vehicle, outlet |  [optional]



