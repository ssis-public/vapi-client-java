

# IndexDealerDetailResponseList

The object with the payload data.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealers** | [**List&lt;IndexDealer&gt;**](IndexDealer.md) |  |  [optional]
**errors** | **List&lt;String&gt;** |  |  [optional]



