

# PaginationParameters

The pagination parameters.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resultCount** | **Integer** | Number of datasets returns. |  [optional]
**resultOffset** | **Integer** | Dataset to begin. |  [optional]
**sortType** | **String** | The column name to sort by. Most fields are supported for sorting |  [optional]
**sortOrderASC** | **Boolean** | The sort direction. If true then ascending. |  [optional]



