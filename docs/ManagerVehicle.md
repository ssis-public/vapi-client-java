

# ManagerVehicle


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lastChange** | **OffsetDateTime** | The last time the vehicle data changed |  [optional] [readonly]
**lastUpdate** | **OffsetDateTime** | The last time the vehicle got send |  [optional] [readonly]
**dealerId** | **Long** | The distributor id. |  [optional] [readonly]
**offerNumber** | **String** | The offer number must be unique. | 
**makeKey** | **String** | The searchable key for the vehicle make. See https://vapi.ssis.de/value/Make for possible values. |  [optional]
**modelKey** | **String** | The searchable key for the vehicle model. See https://vapi.ssis.de/value/Model for possible values. |  [optional]
**makeText** | **String** | The visible text for the vehicle make. |  [optional]
**modelText** | **String** | The visible text for the vehicle model. |  [optional]
**versionText** | **String** | The visible text for the vehicle version. | 
**hsn** | **Integer** | The &#39;Herstellerschl�sselnummer&#39; as provided by the Kraftfahrt-Bundesamt: http://www.kba.de/SharedDocs/Publikationen/DE/Fahrzeugtechnik/SV/sv32_pdf.pdf?__blob&#x3D;publicationFile&amp;v&#x3D;6 |  [optional]
**tsn** | **String** | The &#39;Typschl�sselnummer&#39;. |  [optional]
**modelYear** | **Integer** | The model year describes the version of a model. The model year may or may not be the same as the calendar year in which the product was manufactured. |  [optional]
**enginePowerKw** | **Integer** | The engine power in kilowatt. | 
**engineCylinderCount** | **Integer** | The number of cylinders in the vehicle engine. |  [optional]
**engineCubicCapacity** | **Integer** | The cubic capacity of the vehicle engine. |  [optional]
**gearCount** | **Integer** | The number of gears in the transmission excluding the reverse gear. |  [optional]
**transmissionKey** | **String** | The transmission. See https://vapi.ssis.de/value/Transmission for possible values. |  [optional]
**axeCount** | **Integer** | The number of axes the vehicle has. |  [optional]
**wheelBase** | **Integer** | The distance between the wheels. |  [optional]
**tireSize** | **Integer** | The size of the mounted tires. |  [optional]
**driveTypeKey** | **String** | The drive-type. See https://vapi.ssis.de/value/DriveType for possible values. |  [optional]
**manufacturingYear** | **Integer** | The year the vehicle was build. |  [optional]
**bodyStyleKey** | **String** | The body-style. See https://vapi.ssis.de/value/BodyStyle for possible values. | 
**doorCount** | **Integer** | The number of doors. |  [optional]
**seatCount** | **Integer** | The number of seats. |  [optional]
**vin** | **String** | The vehicle identification number. |  [optional]
**consumerPrice** | **BigDecimal** | The price a consumer has to pay for this vehicle. |  [optional]
**dealerPrice** | **BigDecimal** | The price a vehicle dealer has to pay for this vehicle. |  [optional]
**taxReportable** | **Boolean** | True if the tax is reportable. |  [optional]
**brokerOffer** | **Boolean** | True if this is a broker offer. |  [optional]
**agecategoryKey** | **String** | The age category. See https://vapi.ssis.de/value/Agecategory for possible values. | 
**deliverycategoryKey** | **String** | The delivery category. See https://vapi.ssis.de/value/Deliverycategory for possible values. | 
**deliveryDate** | **LocalDate** | The date on which the car will be delivered to the dealer. |  [optional]
**deliveryTime** | **Integer** | The number of days it takes to deliver the car to the dealer. |  [optional]
**retrievalCountryKey** | **String** | The retrieval country. See https://vapi.ssis.de/value/Country for possible values. |  [optional]
**originCountryKey** | **String** | The origin country. See https://vapi.ssis.de/value/Country for possible values. |  [optional]
**reimport** | **Boolean** | True if the vehicle was sold in a foreign country. |  [optional]
**mileage** | **Integer** | The distance this vehicle was driven in kilometer. |  [optional]
**previousOwnerCount** | **Integer** | The number of owners this car previously had. |  [optional]
**oneDayRegistration** | **Boolean** | True if the car had a one day registration. |  [optional]
**firstRegistrationDate** | **LocalDate** | The date of the first registration. |  [optional]
**warrantyStartDate** | **LocalDate** | The date the warranty started. |  [optional]
**accidented** | **Boolean** | True if the car has accident damage. |  [optional]
**functional** | **Boolean** | True if the car is in a drivable condition and without unrepaired accident damages. |  [optional]
**returnedRental** | **Boolean** | True if the car was previously used by a car rental company. |  [optional]
**returnedLeasing** | **Boolean** | True if the car was previosly leased by someone and is returned after the lease ended. |  [optional]
**serviceRecord** | **Boolean** | True if a documentation of the vehicle inspections is provided. |  [optional]
**generalInspection** | **LocalDate** | The date of the next general inspection. |  [optional]
**nonSmoker** | **Boolean** | True if the vehicle was not smoked in. |  [optional]
**weight** | **Integer** | The vehicle weight in kilogram. |  [optional]
**cocMass** | **Integer** | The vehicle mass as noted in the COC. See https://de.wikipedia.org/wiki/COC_(Zulassung)#Kraftfahrzeuge |  [optional]
**trailerLoad** | **Integer** | The maximum trailer load in kilogram. |  [optional]
**energyEfficiencyClassKey** | **String** | The label for the applicable class of energy consumption, ranges between A+, A, B, C, D, E, F or G. |  [optional]
**nedcEmission** | **BigDecimal** | Indicates the amount of carbon dioxide emissions in grams per kilometer traveled. |  [optional]
**nedcConsumptionInner** | **BigDecimal** | Fuel consumption as measured in specific tests executed in city traffic situations. Number in l/100km (natural gas (CNG) in kg/100km, is ignored for electric vehicles) |  [optional]
**nedcConsumptionOuter** | **BigDecimal** | Fuel consumption as measured in specific tests executed in highway traffic situations. Number in l/100km (natural gas (CNG) in kg/100km, is ignored for electric vehicles) |  [optional]
**nedcConsumptionCombined** | **BigDecimal** | Fuel consumption as measured in specific tests executed in city and highway traffic situations. Number in l/100km (natural gas (CNG) in kg/100km, electric vehicles in kWh/100km) |  [optional]
**nedcConsumptionPowerCombined** | **BigDecimal** | Energy Consumption of electic vehicles in  kWh/100km. |  [optional]
**wltpEmission** | **Integer** | CO2 emission In g/km (0 for electric vehicles). |  [optional]
**wltpConsumptionCombined** | **BigDecimal** | Combined consumption as defined in the WLTP standard. Liquids in l/100km (gas in kg/100km, electic in kWh/100km). |  [optional]
**wltpConsumptionLow** | **BigDecimal** | Low consumption as defined in the WLTP standard. Liquids in l/100km (gas in kg/100km, electic in kWh/100km). |  [optional]
**wltpConsumptionMedium** | **BigDecimal** | Medium consumption as defined in the WLTP standard. Liquids in l/100km (gas in kg/100km, electic in kWh/100km). |  [optional]
**wltpConsumptionHigh** | **BigDecimal** | High consumption as defined in the WLTP standard. Liquids in l/100km (gas in kg/100km, electic in kWh/100km). |  [optional]
**wltpConsumptionExtraHigh** | **BigDecimal** | Extra high consumption as defined in the WLTP standard. Liquids in l/100km (gas in kg/100km, electic in kWh/100km). |  [optional]
**fuelTypeKey** | **String** | The base fuel type. See https://vapi.ssis.de/value/BaseFueltype for possible values. |  [optional]
**primaryFuelKey** | **String** | When you want to provide EnVKV compliant values, you have to specify the exact petrol type, that the consumption values are based on. |  [optional]
**secondaryFuelKey** | **String** | When you want to provide EnVKV compliant values, you have to specify the exact petrol type, that the consumption values are based on. |  [optional]
**emissionStickerKey** | **String** | Emission sticker for German low emission zones (Feinstaubplakette f�r Umweltzone). See https://vapi.ssis.de/value/EmissionSticker for possible values. |  [optional]
**emissionStandardKey** | **String** | EURO 1, 2, 3, 4, ...; See https://vapi.ssis.de/value/EmissionStandard for possible values. |  [optional]
**exteriors** | [**List&lt;Exterior&gt;**](Exterior.md) | A list of exterior choices. | 
**interiorColorKey** | **String** | The searchable key for the interior color. See https://vapi.ssis.de/value/Color for possible values. |  [optional]
**interiorColorText** | **String** | The visible text for the interior color. |  [optional]
**upholsteryKey** | **String** | The searchable key for the interior type. See https://vapi.ssis.de/value/Upholstery for possible values. |  [optional]
**cargoSpaceLength** | **Integer** | The length of the cargo space in millimeter. |  [optional]
**cargoSpaceWidth** | **Integer** | The width of the cargo space in millimeter. |  [optional]
**cargoSpaceHeight** | **Integer** | The height of the cargo space in millimeter. |  [optional]
**additionalNotes** | **String** | additional notes |  [optional]
**images** | [**List&lt;Image&gt;**](Image.md) | The list of images of this vehicle. Upload these images to https://vapi.ssis.de/manage/image |  [optional]
**features** | [**List&lt;Feature&gt;**](Feature.md) | The list of features. |  [optional]
**errors** | [**List&lt;Error&gt;**](Error.md) | All violations of the ELN data integrity rules. |  [optional] [readonly]
**changes** | [**List&lt;Change&gt;**](Change.md) | All enhancements to the vehicle data that were made during data import. |  [optional]
**dataProviderName** | **String** | The name of the company, that provides the data for the dealer. |  [optional]
**dataProviderVehicleId** | **String** | The internal id for this vehicle. Provided by the data provider. |  [optional]
**dataProviderVehicleUrl** | **String** | The url to the original vehicle. Provided by the data provider. |  [optional]
**deliverableImmediately** | **Boolean** | Is true if the vehicle has no delivery time, or the delivery date is in the past. |  [optional] [readonly]
**deliverableOnShortNotice** | **Boolean** | Is true if the vehicle has up to 7 Days delivery time, or the delivery date is up to 7 in the future. |  [optional] [readonly]
**uid** | **String** | A Unique ID for this vehicle. Set by ELN |  [optional] [readonly]



