

# VehicleModelSearch

A a make, model and version combination. The exclude lists are handled with priority.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**makeKey** | **String** | The key of a make. |  [optional]
**modelKey** | **String** | The key of a model. |  [optional]
**versionText** | **String** | A list of versions. |  [optional]



