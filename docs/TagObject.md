

# TagObject

The class for Vehicle Aggregation Tags.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **String** | The Tag. | 
**description** | **String** | The description of the Tag. |  [optional]



