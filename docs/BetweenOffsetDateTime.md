

# BetweenOffsetDateTime

This class is used to enter two values, to search a value is between.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **OffsetDateTime** | The begin value. |  [optional]
**until** | **OffsetDateTime** | The end value. |  [optional]



