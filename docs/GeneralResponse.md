

# GeneralResponse

The response wrapper class for one class.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metaData** | [**MetaData**](MetaData.md) |  | 
**classObject** | **Object** | The object. | 



