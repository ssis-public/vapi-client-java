

# ListResponseClientResponseIntf

The response wrapper class for a list of items.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metaData** | [**MetaData**](MetaData.md) |  |  [optional]
**items** | **List&lt;Object&gt;** | The list of items. | 



