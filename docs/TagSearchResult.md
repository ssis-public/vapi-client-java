

# TagSearchResult

The possible aggregations for your tag.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aggregation** | **Map&lt;String, Map&lt;String, TagObject&gt;&gt;** | The aggragations. |  [optional]



