

# DeleteVehiclesByUidOffernumberExceptRequest

Deletion of all vehicles except those with the transferred values.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uids** | **List&lt;String&gt;** | The vehicles with these Uids should not be deleted. |  [optional]
**offerNumbers** | **List&lt;String&gt;** | The vehicles with these offer number should not be deleted. |  [optional]



