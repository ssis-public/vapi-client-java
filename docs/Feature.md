

# Feature

The list of features.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **String** |  |  [optional]
**price** | **BigDecimal** |  |  [optional]
**searchflags** | **List&lt;String&gt;** |  |  [optional]
**uid** | **String** |  |  [optional]



