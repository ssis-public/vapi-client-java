/*
 * VAPI
 * <b>Build Time: 2022-12-09T17:30:18.023+01:00</b><br><p><a href=\"/public/changelog\" target=\"_blank\">Changelog</a></p><p class=\"clients\">API-Clients:<br><a href=\"https://gitlab.com/ssis-public/vapi-client-php\" target=\"_blank\">PHP-Client</a><br><a href=\"https://gitlab.com/ssis-public/vapi-client-java\" target=\"_blank\">Java-Client</a></p>
 *
 * The version of the OpenAPI document: 6.3.0-374
 * Contact: technik@ssis.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.vapi.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.ssis.vapi.client.swagger.handler.JSON;


/**
 * A a make, model and version combination. The exclude lists are handled with priority.
 */
@ApiModel(description = "A a make, model and version combination. The exclude lists are handled with priority.")
@JsonPropertyOrder({
  VehicleModelSearch.JSON_PROPERTY_MAKE_KEY,
  VehicleModelSearch.JSON_PROPERTY_MODEL_KEY,
  VehicleModelSearch.JSON_PROPERTY_VERSION_TEXT
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class VehicleModelSearch {
  public static final String JSON_PROPERTY_MAKE_KEY = "makeKey";
  private String makeKey;

  public static final String JSON_PROPERTY_MODEL_KEY = "modelKey";
  private String modelKey;

  public static final String JSON_PROPERTY_VERSION_TEXT = "versionText";
  private String versionText;


  public VehicleModelSearch makeKey(String makeKey) {
    this.makeKey = makeKey;
    return this;
  }

   /**
   * The key of a make.
   * @return makeKey
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "\"AUDI\"", value = "The key of a make.")
  @JsonProperty(JSON_PROPERTY_MAKE_KEY)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public String getMakeKey() {
    return makeKey;
  }


  @JsonProperty(JSON_PROPERTY_MAKE_KEY)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setMakeKey(String makeKey) {
    this.makeKey = makeKey;
  }


  public VehicleModelSearch modelKey(String modelKey) {
    this.modelKey = modelKey;
    return this;
  }

   /**
   * The key of a model.
   * @return modelKey
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "\"80\"", value = "The key of a model.")
  @JsonProperty(JSON_PROPERTY_MODEL_KEY)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public String getModelKey() {
    return modelKey;
  }


  @JsonProperty(JSON_PROPERTY_MODEL_KEY)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setModelKey(String modelKey) {
    this.modelKey = modelKey;
  }


  public VehicleModelSearch versionText(String versionText) {
    this.versionText = versionText;
    return this;
  }

   /**
   * A list of versions.
   * @return versionText
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "\"80\"", value = "A list of versions.")
  @JsonProperty(JSON_PROPERTY_VERSION_TEXT)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public String getVersionText() {
    return versionText;
  }


  @JsonProperty(JSON_PROPERTY_VERSION_TEXT)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setVersionText(String versionText) {
    this.versionText = versionText;
  }


  /**
   * Return true if this VehicleModelSearch object is equal to o.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VehicleModelSearch vehicleModelSearch = (VehicleModelSearch) o;
    return Objects.equals(this.makeKey, vehicleModelSearch.makeKey) &&
        Objects.equals(this.modelKey, vehicleModelSearch.modelKey) &&
        Objects.equals(this.versionText, vehicleModelSearch.versionText);
  }

  @Override
  public int hashCode() {
    return Objects.hash(makeKey, modelKey, versionText);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VehicleModelSearch {\n");
    sb.append("    makeKey: ").append(toIndentedString(makeKey)).append("\n");
    sb.append("    modelKey: ").append(toIndentedString(modelKey)).append("\n");
    sb.append("    versionText: ").append(toIndentedString(versionText)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

