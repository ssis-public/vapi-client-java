

# VehicleUidListItem


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **String** |  |  [optional]
**dateModified** | **OffsetDateTime** |  |  [optional]
**lastUpdate** | **OffsetDateTime** |  |  [optional]



