

# InquiryRequestor

The personal data of the requestor.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerId** | **Long** |  |  [optional]
**requestorType** | [**RequestorTypeEnum**](#RequestorTypeEnum) | Contains the request values. |  [optional]
**companyname** | **String** | If you are a company. |  [optional]
**requestorSalutation** | [**RequestorSalutationEnum**](#RequestorSalutationEnum) | Contains the request values. |  [optional]
**firstname** | **String** |  |  [optional]
**lastname** | **String** |  | 
**street** | **String** |  |  [optional]
**zipcode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**countryIso2** | **String** |  |  [optional]
**email** | **String** |  | 
**phone** | **String** |  |  [optional]
**fax** | **String** |  |  [optional]
**mobile** | **String** |  |  [optional]



## Enum: RequestorTypeEnum

Name | Value
---- | -----
COMPANY | &quot;COMPANY&quot;
PRIVATE | &quot;PRIVATE&quot;



## Enum: RequestorSalutationEnum

Name | Value
---- | -----
MR | &quot;Mr&quot;
MRS | &quot;Mrs&quot;



