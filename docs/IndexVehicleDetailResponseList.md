

# IndexVehicleDetailResponseList

Vehicle Detail Response.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicles** | [**List&lt;IndexVehicle&gt;**](IndexVehicle.md) | List of Vehicles Details. |  [optional]
**duplicatesRemoved** | **Integer** | Number of duplicate uids which was sent, has been removed. |  [optional]



