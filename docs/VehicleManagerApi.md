# VehicleManagerApi

All URIs are relative to *https://vapi.ssis.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAllVehicles**](VehicleManagerApi.md#deleteAllVehicles) | **DELETE** /manage/vehicle/all | delete all Vehicles
[**deleteAllVehiclesExcept**](VehicleManagerApi.md#deleteAllVehiclesExcept) | **POST** /manage/vehicle/delete/except | delete all Vehicles except the ones with the given uids or offernumbers
[**deleteVehicleByOfferNumber**](VehicleManagerApi.md#deleteVehicleByOfferNumber) | **DELETE** /manage/vehicle | delete the Vehicle with the given offerNumber
[**deleteVehicleByUid**](VehicleManagerApi.md#deleteVehicleByUid) | **DELETE** /manage/vehicle/{uid} | delete the Vehicle with the given vehicleUid
[**getVehicle**](VehicleManagerApi.md#getVehicle) | **GET** /manage/vehicle/{uid} | get the Vehicle with the given vehicleUid
[**getVehicleUids**](VehicleManagerApi.md#getVehicleUids) | **GET** /manage/vehicle | get a list of all vehicleUids that belong to the current Dealer
[**getVehicleUidsWithFlatParameters**](VehicleManagerApi.md#getVehicleUidsWithFlatParameters) | **GET** /manage/vehicle/flat_parameters | get a list of all vehicleUids that belong to the current Dealer
[**saveVehicle**](VehicleManagerApi.md#saveVehicle) | **POST** /manage/vehicle | save a Vehicle



## deleteAllVehicles

> ClientResponseCommonResponse deleteAllVehicles()

delete all Vehicles

delete all Vehicles

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleManagerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleManagerApi apiInstance = new VehicleManagerApi(defaultClient);
        try {
            ClientResponseCommonResponse result = apiInstance.deleteAllVehicles();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleManagerApi#deleteAllVehicles");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**ClientResponseCommonResponse**](ClientResponseCommonResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## deleteAllVehiclesExcept

> ClientResponseObject deleteAllVehiclesExcept(deleteVehiclesByUidOffernumberExceptRequest)

delete all Vehicles except the ones with the given uids or offernumbers

delete all Vehicles except

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleManagerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleManagerApi apiInstance = new VehicleManagerApi(defaultClient);
        DeleteVehiclesByUidOffernumberExceptRequest deleteVehiclesByUidOffernumberExceptRequest = new DeleteVehiclesByUidOffernumberExceptRequest(); // DeleteVehiclesByUidOffernumberExceptRequest | 
        try {
            ClientResponseObject result = apiInstance.deleteAllVehiclesExcept(deleteVehiclesByUidOffernumberExceptRequest);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleManagerApi#deleteAllVehiclesExcept");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deleteVehiclesByUidOffernumberExceptRequest** | [**DeleteVehiclesByUidOffernumberExceptRequest**](DeleteVehiclesByUidOffernumberExceptRequest.md)|  |

### Return type

[**ClientResponseObject**](ClientResponseObject.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## deleteVehicleByOfferNumber

> ClientResponseCommonResponse deleteVehicleByOfferNumber(offerNumber)

delete the Vehicle with the given offerNumber

delete the Vehicle with the given offerNumber

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleManagerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleManagerApi apiInstance = new VehicleManagerApi(defaultClient);
        String offerNumber = "offerNumber_example"; // String | 
        try {
            ClientResponseCommonResponse result = apiInstance.deleteVehicleByOfferNumber(offerNumber);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleManagerApi#deleteVehicleByOfferNumber");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offerNumber** | **String**|  |

### Return type

[**ClientResponseCommonResponse**](ClientResponseCommonResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## deleteVehicleByUid

> ClientResponseCommonResponse deleteVehicleByUid(uid)

delete the Vehicle with the given vehicleUid

delete the Vehicle with the given vehicleUid

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleManagerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleManagerApi apiInstance = new VehicleManagerApi(defaultClient);
        String uid = "uid_example"; // String | 
        try {
            ClientResponseCommonResponse result = apiInstance.deleteVehicleByUid(uid);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleManagerApi#deleteVehicleByUid");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **String**|  |

### Return type

[**ClientResponseCommonResponse**](ClientResponseCommonResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## getVehicle

> ClientResponseManagerVehicle getVehicle(uid)

get the Vehicle with the given vehicleUid

get the Vehicle with the given vehicle

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleManagerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleManagerApi apiInstance = new VehicleManagerApi(defaultClient);
        String uid = "uid_example"; // String | 
        try {
            ClientResponseManagerVehicle result = apiInstance.getVehicle(uid);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleManagerApi#getVehicle");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **String**|  |

### Return type

[**ClientResponseManagerVehicle**](ClientResponseManagerVehicle.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## getVehicleUids

> ClientResponseVehicleUidList getVehicleUids(filter, pagination)

get a list of all vehicleUids that belong to the current Dealer

get a list of all vehicleUids that belong to the current Dealer

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleManagerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleManagerApi apiInstance = new VehicleManagerApi(defaultClient);
        ManagerVehicleFilterParameters filter = new ManagerVehicleFilterParameters(); // ManagerVehicleFilterParameters | 
        PaginationParameters pagination = new PaginationParameters(); // PaginationParameters | 
        try {
            ClientResponseVehicleUidList result = apiInstance.getVehicleUids(filter, pagination);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleManagerApi#getVehicleUids");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**ManagerVehicleFilterParameters**](ManagerVehicleFilterParameters.md)|  |
 **pagination** | [**PaginationParameters**](PaginationParameters.md)|  |

### Return type

[**ClientResponseVehicleUidList**](ClientResponseVehicleUidList.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## getVehicleUidsWithFlatParameters

> ClientResponseVehicleUidList getVehicleUidsWithFlatParameters(offerNumber, changedBefore, changedAfter, sendBefore, sendAfter, hasErrors, makeKey, modelKey, minDealerPrice, maxDealerPrice, minConsumerPrice, maxConsumerPrice, resultCount, resultOffset, sortType, sortOrderASC)

get a list of all vehicleUids that belong to the current Dealer

get a list of all vehicleUids that belong to the current Dealer

### Example

```java
import java.math.BigDecimal;
import java.time.OffsetDateTime;
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleManagerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleManagerApi apiInstance = new VehicleManagerApi(defaultClient);
        String offerNumber = "offerNumber_example"; // String | 
        OffsetDateTime changedBefore = OffsetDateTime.now(); // OffsetDateTime | 
        OffsetDateTime changedAfter = OffsetDateTime.now(); // OffsetDateTime | 
        OffsetDateTime sendBefore = OffsetDateTime.now(); // OffsetDateTime | 
        OffsetDateTime sendAfter = OffsetDateTime.now(); // OffsetDateTime | 
        Boolean hasErrors = true; // Boolean | 
        String makeKey = "makeKey_example"; // String | 
        String modelKey = "modelKey_example"; // String | 
        BigDecimal minDealerPrice = new BigDecimal(78); // BigDecimal | 
        BigDecimal maxDealerPrice = new BigDecimal(78); // BigDecimal | 
        BigDecimal minConsumerPrice = new BigDecimal(78); // BigDecimal | 
        BigDecimal maxConsumerPrice = new BigDecimal(78); // BigDecimal | 
        Integer resultCount = 56; // Integer | 
        Integer resultOffset = 56; // Integer | 
        String sortType = "sortType_example"; // String | 
        Boolean sortOrderASC = true; // Boolean | 
        try {
            ClientResponseVehicleUidList result = apiInstance.getVehicleUidsWithFlatParameters(offerNumber, changedBefore, changedAfter, sendBefore, sendAfter, hasErrors, makeKey, modelKey, minDealerPrice, maxDealerPrice, minConsumerPrice, maxConsumerPrice, resultCount, resultOffset, sortType, sortOrderASC);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleManagerApi#getVehicleUidsWithFlatParameters");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offerNumber** | **String**|  | [optional]
 **changedBefore** | **OffsetDateTime**|  | [optional]
 **changedAfter** | **OffsetDateTime**|  | [optional]
 **sendBefore** | **OffsetDateTime**|  | [optional]
 **sendAfter** | **OffsetDateTime**|  | [optional]
 **hasErrors** | **Boolean**|  | [optional]
 **makeKey** | **String**|  | [optional]
 **modelKey** | **String**|  | [optional]
 **minDealerPrice** | **BigDecimal**|  | [optional]
 **maxDealerPrice** | **BigDecimal**|  | [optional]
 **minConsumerPrice** | **BigDecimal**|  | [optional]
 **maxConsumerPrice** | **BigDecimal**|  | [optional]
 **resultCount** | **Integer**|  | [optional]
 **resultOffset** | **Integer**|  | [optional]
 **sortType** | **String**|  | [optional]
 **sortOrderASC** | **Boolean**|  | [optional]

### Return type

[**ClientResponseVehicleUidList**](ClientResponseVehicleUidList.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## saveVehicle

> ClientResponseVehicleSaveResponse saveVehicle(managerVehicle)

save a Vehicle

save a Vehicle

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleManagerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleManagerApi apiInstance = new VehicleManagerApi(defaultClient);
        ManagerVehicle managerVehicle = new ManagerVehicle(); // ManagerVehicle | 
        try {
            ClientResponseVehicleSaveResponse result = apiInstance.saveVehicle(managerVehicle);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleManagerApi#saveVehicle");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managerVehicle** | [**ManagerVehicle**](ManagerVehicle.md)|  |

### Return type

[**ClientResponseVehicleSaveResponse**](ClientResponseVehicleSaveResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |

