

# BetweenInteger

This class is used to enter two values, to search a value is between.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **Integer** | The begin value. |  [optional]
**until** | **Integer** | The end value. |  [optional]



