

# VehicleSaveResponse

The object with the payload data.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **String** |  |  [optional]
**changes** | [**List&lt;Change&gt;**](Change.md) |  |  [optional]
**errors** | [**List&lt;Error&gt;**](Error.md) |  |  [optional]



