

# IndexDealerSetting

A dealer setting.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | A unique key of the setting. |  [optional]
**description** | **String** | The description of the setting. |  [optional]
**dealerValue** | **String** | The specific dealer setting value. |  [optional]
**defaultValue** | **String** | A default value for this setting. |  [optional]
**lastUpdate** | **OffsetDateTime** | The last update date of this setting. |  [optional]



