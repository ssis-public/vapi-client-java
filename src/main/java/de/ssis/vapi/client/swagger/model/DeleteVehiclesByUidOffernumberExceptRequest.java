/*
 * VAPI
 * <b>Build Time: 2022-12-09T17:30:18.023+01:00</b><br><p><a href=\"/public/changelog\" target=\"_blank\">Changelog</a></p><p class=\"clients\">API-Clients:<br><a href=\"https://gitlab.com/ssis-public/vapi-client-php\" target=\"_blank\">PHP-Client</a><br><a href=\"https://gitlab.com/ssis-public/vapi-client-java\" target=\"_blank\">Java-Client</a></p>
 *
 * The version of the OpenAPI document: 6.3.0-374
 * Contact: technik@ssis.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.vapi.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.ssis.vapi.client.swagger.handler.JSON;


/**
 * Deletion of all vehicles except those with the transferred values.
 */
@ApiModel(description = "Deletion of all vehicles except those with the transferred values.")
@JsonPropertyOrder({
  DeleteVehiclesByUidOffernumberExceptRequest.JSON_PROPERTY_UIDS,
  DeleteVehiclesByUidOffernumberExceptRequest.JSON_PROPERTY_OFFER_NUMBERS
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class DeleteVehiclesByUidOffernumberExceptRequest {
  public static final String JSON_PROPERTY_UIDS = "uids";
  private List<String> uids = null;

  public static final String JSON_PROPERTY_OFFER_NUMBERS = "offerNumbers";
  private List<String> offerNumbers = null;


  public DeleteVehiclesByUidOffernumberExceptRequest uids(List<String> uids) {
    this.uids = uids;
    return this;
  }

  public DeleteVehiclesByUidOffernumberExceptRequest addUidsItem(String uidsItem) {
    if (this.uids == null) {
      this.uids = new ArrayList<>();
    }
    this.uids.add(uidsItem);
    return this;
  }

   /**
   * The vehicles with these Uids should not be deleted.
   * @return uids
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "The vehicles with these Uids should not be deleted.")
  @JsonProperty(JSON_PROPERTY_UIDS)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public List<String> getUids() {
    return uids;
  }


  @JsonProperty(JSON_PROPERTY_UIDS)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setUids(List<String> uids) {
    this.uids = uids;
  }


  public DeleteVehiclesByUidOffernumberExceptRequest offerNumbers(List<String> offerNumbers) {
    this.offerNumbers = offerNumbers;
    return this;
  }

  public DeleteVehiclesByUidOffernumberExceptRequest addOfferNumbersItem(String offerNumbersItem) {
    if (this.offerNumbers == null) {
      this.offerNumbers = new ArrayList<>();
    }
    this.offerNumbers.add(offerNumbersItem);
    return this;
  }

   /**
   * The vehicles with these offer number should not be deleted.
   * @return offerNumbers
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "The vehicles with these offer number should not be deleted.")
  @JsonProperty(JSON_PROPERTY_OFFER_NUMBERS)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public List<String> getOfferNumbers() {
    return offerNumbers;
  }


  @JsonProperty(JSON_PROPERTY_OFFER_NUMBERS)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setOfferNumbers(List<String> offerNumbers) {
    this.offerNumbers = offerNumbers;
  }


  /**
   * Return true if this DeleteVehiclesByUidOffernumberExceptRequest object is equal to o.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeleteVehiclesByUidOffernumberExceptRequest deleteVehiclesByUidOffernumberExceptRequest = (DeleteVehiclesByUidOffernumberExceptRequest) o;
    return Objects.equals(this.uids, deleteVehiclesByUidOffernumberExceptRequest.uids) &&
        Objects.equals(this.offerNumbers, deleteVehiclesByUidOffernumberExceptRequest.offerNumbers);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uids, offerNumbers);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeleteVehiclesByUidOffernumberExceptRequest {\n");
    sb.append("    uids: ").append(toIndentedString(uids)).append("\n");
    sb.append("    offerNumbers: ").append(toIndentedString(offerNumbers)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

