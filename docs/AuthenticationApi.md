# AuthenticationApi

All URIs are relative to *https://vapi.ssis.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getNewToken**](AuthenticationApi.md#getNewToken) | **GET** /public/getNewToken | Get a new Token only if the given Token is valid. Otherwise a new login is required.
[**login**](AuthenticationApi.md#login) | **POST** /public/login | Login the given API-User and return a Token.



## getNewToken

> JWTTokenResponse getNewToken()

Get a new Token only if the given Token is valid. Otherwise a new login is required.

Get a new Token only if the given Token is valid. Otherwise a new login is required.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.AuthenticationApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AuthenticationApi apiInstance = new AuthenticationApi(defaultClient);
        try {
            JWTTokenResponse result = apiInstance.getNewToken();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AuthenticationApi#getNewToken");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**JWTTokenResponse**](JWTTokenResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## login

> JWTTokenResponse login(authenticationRequest)

Login the given API-User and return a Token.

Login the given API-User and return a Token.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.AuthenticationApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AuthenticationApi apiInstance = new AuthenticationApi(defaultClient);
        AuthenticationRequest authenticationRequest = new AuthenticationRequest(); // AuthenticationRequest | 
        try {
            JWTTokenResponse result = apiInstance.login(authenticationRequest);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AuthenticationApi#login");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authenticationRequest** | [**AuthenticationRequest**](AuthenticationRequest.md)|  |

### Return type

[**JWTTokenResponse**](JWTTokenResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Username or password wrong. |  -  |
| **0** | Other error responses. |  -  |

