

# ClassListResponse

The response wrapper class for a list of classes.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metaData** | [**MetaData**](MetaData.md) |  |  [optional]
**items** | **List&lt;String&gt;** | The list of classes. | 



