/*
 * VAPI
 * <b>Build Time: 2022-12-09T17:30:18.023+01:00</b><br><p><a href=\"/public/changelog\" target=\"_blank\">Changelog</a></p><p class=\"clients\">API-Clients:<br><a href=\"https://gitlab.com/ssis-public/vapi-client-php\" target=\"_blank\">PHP-Client</a><br><a href=\"https://gitlab.com/ssis-public/vapi-client-java\" target=\"_blank\">Java-Client</a></p>
 *
 * The version of the OpenAPI document: 6.3.0-374
 * Contact: technik@ssis.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.vapi.client.swagger.api;

import de.ssis.vapi.client.swagger.handler.*;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.model.ClassListResponse;
import de.ssis.vapi.client.swagger.model.ErrorDetails;
import de.ssis.vapi.client.swagger.model.GeneralResponse;
import de.ssis.vapi.client.swagger.model.ListResponseClientResponseIntf;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for StaticValuesApi
 */
public class StaticValuesApiTest {

    private final StaticValuesApi api = new StaticValuesApi();

    /**
     * Get all class names for static values.
     *
     * Get all class names for static values.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getAllStaticValueClassesTest() throws ApiException {
        //ClassListResponse response = api.getAllStaticValueClasses();
        // TODO: test validations
    }

    /**
     * Get all models of given make.
     *
     * Get all models of given make.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getModelsByMakeTest() throws ApiException {
        //String make = null;
        //ListResponseClientResponseIntf response = api.getModelsByMake(make);
        // TODO: test validations
    }

    /**
     * Get values of given static value class and key.
     *
     * Get values of given static value class and key.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getStaticValueContentByKeyTest() throws ApiException {
        //String staticValueClassName = null;
        //String staticValueKey = null;
        //GeneralResponse response = api.getStaticValueContentByKey(staticValueClassName, staticValueKey);
        // TODO: test validations
    }

    /**
     * Get all values of given static value class.
     *
     * Get all values of given static value class.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getStaticValueContentsTest() throws ApiException {
        //String staticValueClassName = null;
        //ListResponseClientResponseIntf response = api.getStaticValueContents(staticValueClassName);
        // TODO: test validations
    }

}
