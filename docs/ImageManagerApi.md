# ImageManagerApi

All URIs are relative to *https://vapi.ssis.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saveImage**](ImageManagerApi.md#saveImage) | **POST** /manage/image | Saves an image



## saveImage

> ClientResponseCommonResponse saveImage(branded, file)

Saves an image

Saves an image to the image pool of the current Dealer

### Example

```java
import java.io.File;
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.ImageManagerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        ImageManagerApi apiInstance = new ImageManagerApi(defaultClient);
        Boolean branded = true; // Boolean | 
        File file = new File("/path/to/file"); // File | 
        try {
            ClientResponseCommonResponse result = apiInstance.saveImage(branded, file);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling ImageManagerApi#saveImage");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **branded** | **Boolean**|  | [optional]
 **file** | **File**|  |

### Return type

[**ClientResponseCommonResponse**](ClientResponseCommonResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: */*, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |

