# VehicleApi

All URIs are relative to *https://vapi.ssis.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**findAllVehicleIdsFromAuthUser**](VehicleApi.md#findAllVehicleIdsFromAuthUser) | **GET** /vehicle | VehicleApi.findAllVehicleIdsFromAuthUser(integer resultCount, integer resultOffset, string sortType, boolean sortOrderASC)
[**findVehicleIds**](VehicleApi.md#findVehicleIds) | **POST** /vehicle | VehicleApi.findVehicleIds(IndexVehicleSearchRequest indexVehicleSearchRequest)
[**getVehicleAggregationTags**](VehicleApi.md#getVehicleAggregationTags) | **GET** /vehicle/aggregationtags/{tags} | VehicleApi.getVehicleAggregationTags(array[string] tags)
[**getVehicleDetails**](VehicleApi.md#getVehicleDetails) | **GET** /vehicle/details | VehicleApi.getVehicleDetails(array[string] uids)



## findAllVehicleIdsFromAuthUser

> ClientResponseIndexVehicleSearchResponseList findAllVehicleIdsFromAuthUser(resultCount, resultOffset, sortType, sortOrderASC)

VehicleApi.findAllVehicleIdsFromAuthUser(integer resultCount, integer resultOffset, string sortType, boolean sortOrderASC)

Get all your vehicles.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleApi apiInstance = new VehicleApi(defaultClient);
        Integer resultCount = 10; // Integer | 
        Integer resultOffset = 0; // Integer | 
        String sortType = "uid"; // String | 
        Boolean sortOrderASC = true; // Boolean | 
        try {
            ClientResponseIndexVehicleSearchResponseList result = apiInstance.findAllVehicleIdsFromAuthUser(resultCount, resultOffset, sortType, sortOrderASC);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleApi#findAllVehicleIdsFromAuthUser");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resultCount** | **Integer**|  | [optional] [default to 10]
 **resultOffset** | **Integer**|  | [optional] [default to 0]
 **sortType** | **String**|  | [optional] [default to uid]
 **sortOrderASC** | **Boolean**|  | [optional] [default to true]

### Return type

[**ClientResponseIndexVehicleSearchResponseList**](ClientResponseIndexVehicleSearchResponseList.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## findVehicleIds

> ClientResponseIndexVehicleSearchResponseList findVehicleIds(indexVehicleSearchRequest)

VehicleApi.findVehicleIds(IndexVehicleSearchRequest indexVehicleSearchRequest)

Search for vehicles.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleApi apiInstance = new VehicleApi(defaultClient);
        IndexVehicleSearchRequest indexVehicleSearchRequest = new IndexVehicleSearchRequest(); // IndexVehicleSearchRequest | 
        try {
            ClientResponseIndexVehicleSearchResponseList result = apiInstance.findVehicleIds(indexVehicleSearchRequest);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleApi#findVehicleIds");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **indexVehicleSearchRequest** | [**IndexVehicleSearchRequest**](IndexVehicleSearchRequest.md)|  |

### Return type

[**ClientResponseIndexVehicleSearchResponseList**](ClientResponseIndexVehicleSearchResponseList.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## getVehicleAggregationTags

> ClientResponseTagSearchResult getVehicleAggregationTags(tags)

VehicleApi.getVehicleAggregationTags(array[string] tags)

Get vehicle aggregation tags. Send 'ALL' for an tag overview.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleApi apiInstance = new VehicleApi(defaultClient);
        List<String> tags = Arrays.asList(); // List<String> | 
        try {
            ClientResponseTagSearchResult result = apiInstance.getVehicleAggregationTags(tags);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleApi#getVehicleAggregationTags");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tags** | **List&lt;String&gt;**|  |

### Return type

[**ClientResponseTagSearchResult**](ClientResponseTagSearchResult.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## getVehicleDetails

> ClientResponseIndexVehicleDetailResponseList getVehicleDetails(uids)

VehicleApi.getVehicleDetails(array[string] uids)

Get details for several vehicles.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.VehicleApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        VehicleApi apiInstance = new VehicleApi(defaultClient);
        List<String> uids = Arrays.asList(); // List<String> | 
        try {
            ClientResponseIndexVehicleDetailResponseList result = apiInstance.getVehicleDetails(uids);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling VehicleApi#getVehicleDetails");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uids** | **List&lt;String&gt;**|  |

### Return type

[**ClientResponseIndexVehicleDetailResponseList**](ClientResponseIndexVehicleDetailResponseList.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |

