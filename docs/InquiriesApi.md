# InquiriesApi

All URIs are relative to *https://vapi.ssis.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create**](InquiriesApi.md#create) | **PUT** /inquiry/create | Create an inquiry.



## create

> ClientResponseCommonResponse create(inquiryRequest)

Create an inquiry.

Create an inquiry.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.InquiriesApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        InquiriesApi apiInstance = new InquiriesApi(defaultClient);
        InquiryRequest inquiryRequest = new InquiryRequest(); // InquiryRequest | Create an inquiry.
        try {
            ClientResponseCommonResponse result = apiInstance.create(inquiryRequest);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling InquiriesApi#create");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inquiryRequest** | [**InquiryRequest**](InquiryRequest.md)| Create an inquiry. |

### Return type

[**ClientResponseCommonResponse**](ClientResponseCommonResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Updated |  -  |
| **201** | Created |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |

