

# IndexVehicleFeature

Additional vehicle features.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **String** |  |  [optional]
**text** | **String** |  |  [optional]
**purchasePrice** | **Double** |  |  [optional]
**netSalesPrice** | **Double** |  |  [optional]
**grossSalesPrice** | **Double** |  |  [optional]
**blacklisted** | **Boolean** |  |  [optional]
**searchflags** | **List&lt;String&gt;** |  |  [optional]
**salesPrice** | **Double** |  |  [optional] [readonly]



