

# ManagerVehicleFilterParameters


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offerNumber** | **String** |  |  [optional]
**changedBefore** | **OffsetDateTime** |  |  [optional]
**changedAfter** | **OffsetDateTime** |  |  [optional]
**sendBefore** | **OffsetDateTime** |  |  [optional]
**sendAfter** | **OffsetDateTime** |  |  [optional]
**hasErrors** | **Boolean** |  |  [optional]
**makeKey** | **String** |  |  [optional]
**modelKey** | **String** |  |  [optional]
**minDealerPrice** | **BigDecimal** |  |  [optional]
**maxDealerPrice** | **BigDecimal** |  |  [optional]
**minConsumerPrice** | **BigDecimal** |  |  [optional]
**maxConsumerPrice** | **BigDecimal** |  |  [optional]



