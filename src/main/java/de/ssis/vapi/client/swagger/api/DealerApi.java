package de.ssis.vapi.client.swagger.api;

import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiResponse;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.Pair;

import javax.ws.rs.core.GenericType;

import de.ssis.vapi.client.swagger.model.ClientResponseIndexDealerDetailResponseList;
import de.ssis.vapi.client.swagger.model.ErrorDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class DealerApi {
  private ApiClient apiClient;

  public DealerApi() {
    this(Configuration.getDefaultApiClient());
  }

  public DealerApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Get the API cilent
   *
   * @return API client
   */
  public ApiClient getApiClient() {
    return apiClient;
  }

  /**
   * Set the API cilent
   *
   * @param apiClient an instance of API client
   */
  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * DealerApi.info()
   * Get details for logged in dealer.
   * @return ClientResponseIndexDealerDetailResponseList
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseIndexDealerDetailResponseList getLoggedInDealerDetails() throws ApiException {
    return getLoggedInDealerDetailsWithHttpInfo().getData();
  }

  /**
   * DealerApi.info()
   * Get details for logged in dealer.
   * @return ApiResponse&lt;ClientResponseIndexDealerDetailResponseList&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseIndexDealerDetailResponseList> getLoggedInDealerDetailsWithHttpInfo() throws ApiException {
    Object localVarPostBody = null;
    
    // create path and map variables
    String localVarPath = "/dealer/info";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseIndexDealerDetailResponseList> localVarReturnType = new GenericType<ClientResponseIndexDealerDetailResponseList>() {};

    return apiClient.invokeAPI("DealerApi.getLoggedInDealerDetails", localVarPath, "GET", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
}
