

# ClientResponseVehicleSaveResponse

The general response wrapper.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metaData** | [**MetaData**](MetaData.md) |  | 
**response** | [**VehicleSaveResponse**](VehicleSaveResponse.md) |  | 



