

# IndexVehicleSearchRequest

Request wrapper for vehicle search.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter** | [**VehicleFilter**](VehicleFilter.md) |  |  [optional]
**aggregationTypes** | **List&lt;String&gt;** | Define your aggregation. |  [optional]
**pagination** | [**PaginationParameters**](PaginationParameters.md) |  |  [optional]



