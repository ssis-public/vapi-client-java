

# Image

The list of images of this vehicle. Upload these images to https://vapi.ssis.de/manage/image

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**optional** | **Boolean** |  |  [optional]



