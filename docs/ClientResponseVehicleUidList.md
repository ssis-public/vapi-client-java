

# ClientResponseVehicleUidList

The general response wrapper.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metaData** | [**MetaData**](MetaData.md) |  | 
**response** | [**VehicleUidList**](VehicleUidList.md) |  | 



