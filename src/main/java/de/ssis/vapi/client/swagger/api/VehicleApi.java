package de.ssis.vapi.client.swagger.api;

import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiResponse;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.Pair;

import javax.ws.rs.core.GenericType;

import de.ssis.vapi.client.swagger.model.ClientResponseIndexVehicleDetailResponseList;
import de.ssis.vapi.client.swagger.model.ClientResponseIndexVehicleSearchResponseList;
import de.ssis.vapi.client.swagger.model.ClientResponseTagSearchResult;
import de.ssis.vapi.client.swagger.model.ErrorDetails;
import de.ssis.vapi.client.swagger.model.IndexVehicleSearchRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class VehicleApi {
  private ApiClient apiClient;

  public VehicleApi() {
    this(Configuration.getDefaultApiClient());
  }

  public VehicleApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Get the API cilent
   *
   * @return API client
   */
  public ApiClient getApiClient() {
    return apiClient;
  }

  /**
   * Set the API cilent
   *
   * @param apiClient an instance of API client
   */
  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * VehicleApi.findAllVehicleIdsFromAuthUser(integer resultCount, integer resultOffset, string sortType, boolean sortOrderASC)
   * Get all your vehicles.
   * @param resultCount  (optional, default to 10)
   * @param resultOffset  (optional, default to 0)
   * @param sortType  (optional, default to uid)
   * @param sortOrderASC  (optional, default to true)
   * @return ClientResponseIndexVehicleSearchResponseList
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseIndexVehicleSearchResponseList findAllVehicleIdsFromAuthUser(Integer resultCount, Integer resultOffset, String sortType, Boolean sortOrderASC) throws ApiException {
    return findAllVehicleIdsFromAuthUserWithHttpInfo(resultCount, resultOffset, sortType, sortOrderASC).getData();
  }

  /**
   * VehicleApi.findAllVehicleIdsFromAuthUser(integer resultCount, integer resultOffset, string sortType, boolean sortOrderASC)
   * Get all your vehicles.
   * @param resultCount  (optional, default to 10)
   * @param resultOffset  (optional, default to 0)
   * @param sortType  (optional, default to uid)
   * @param sortOrderASC  (optional, default to true)
   * @return ApiResponse&lt;ClientResponseIndexVehicleSearchResponseList&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseIndexVehicleSearchResponseList> findAllVehicleIdsFromAuthUserWithHttpInfo(Integer resultCount, Integer resultOffset, String sortType, Boolean sortOrderASC) throws ApiException {
    Object localVarPostBody = null;
    
    // create path and map variables
    String localVarPath = "/vehicle";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "resultCount", resultCount));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "resultOffset", resultOffset));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sortType", sortType));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sortOrderASC", sortOrderASC));

    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseIndexVehicleSearchResponseList> localVarReturnType = new GenericType<ClientResponseIndexVehicleSearchResponseList>() {};

    return apiClient.invokeAPI("VehicleApi.findAllVehicleIdsFromAuthUser", localVarPath, "GET", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * VehicleApi.findVehicleIds(IndexVehicleSearchRequest indexVehicleSearchRequest)
   * Search for vehicles.
   * @param indexVehicleSearchRequest  (required)
   * @return ClientResponseIndexVehicleSearchResponseList
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseIndexVehicleSearchResponseList findVehicleIds(IndexVehicleSearchRequest indexVehicleSearchRequest) throws ApiException {
    return findVehicleIdsWithHttpInfo(indexVehicleSearchRequest).getData();
  }

  /**
   * VehicleApi.findVehicleIds(IndexVehicleSearchRequest indexVehicleSearchRequest)
   * Search for vehicles.
   * @param indexVehicleSearchRequest  (required)
   * @return ApiResponse&lt;ClientResponseIndexVehicleSearchResponseList&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseIndexVehicleSearchResponseList> findVehicleIdsWithHttpInfo(IndexVehicleSearchRequest indexVehicleSearchRequest) throws ApiException {
    Object localVarPostBody = indexVehicleSearchRequest;
    
    // verify the required parameter 'indexVehicleSearchRequest' is set
    if (indexVehicleSearchRequest == null) {
      throw new ApiException(400, "Missing the required parameter 'indexVehicleSearchRequest' when calling findVehicleIds");
    }
    
    // create path and map variables
    String localVarPath = "/vehicle";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseIndexVehicleSearchResponseList> localVarReturnType = new GenericType<ClientResponseIndexVehicleSearchResponseList>() {};

    return apiClient.invokeAPI("VehicleApi.findVehicleIds", localVarPath, "POST", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * VehicleApi.getVehicleAggregationTags(array[string] tags)
   * Get vehicle aggregation tags. Send &#39;ALL&#39; for an tag overview.
   * @param tags  (required)
   * @return ClientResponseTagSearchResult
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseTagSearchResult getVehicleAggregationTags(List<String> tags) throws ApiException {
    return getVehicleAggregationTagsWithHttpInfo(tags).getData();
  }

  /**
   * VehicleApi.getVehicleAggregationTags(array[string] tags)
   * Get vehicle aggregation tags. Send &#39;ALL&#39; for an tag overview.
   * @param tags  (required)
   * @return ApiResponse&lt;ClientResponseTagSearchResult&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseTagSearchResult> getVehicleAggregationTagsWithHttpInfo(List<String> tags) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'tags' is set
    if (tags == null) {
      throw new ApiException(400, "Missing the required parameter 'tags' when calling getVehicleAggregationTags");
    }
    
    // create path and map variables
    String localVarPath = "/vehicle/aggregationtags/{tags}"
      .replaceAll("\\{" + "tags" + "\\}", apiClient.escapeString(tags.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseTagSearchResult> localVarReturnType = new GenericType<ClientResponseTagSearchResult>() {};

    return apiClient.invokeAPI("VehicleApi.getVehicleAggregationTags", localVarPath, "GET", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * VehicleApi.getVehicleDetails(array[string] uids)
   * Get details for several vehicles.
   * @param uids  (required)
   * @return ClientResponseIndexVehicleDetailResponseList
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseIndexVehicleDetailResponseList getVehicleDetails(List<String> uids) throws ApiException {
    return getVehicleDetailsWithHttpInfo(uids).getData();
  }

  /**
   * VehicleApi.getVehicleDetails(array[string] uids)
   * Get details for several vehicles.
   * @param uids  (required)
   * @return ApiResponse&lt;ClientResponseIndexVehicleDetailResponseList&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseIndexVehicleDetailResponseList> getVehicleDetailsWithHttpInfo(List<String> uids) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'uids' is set
    if (uids == null) {
      throw new ApiException(400, "Missing the required parameter 'uids' when calling getVehicleDetails");
    }
    
    // create path and map variables
    String localVarPath = "/vehicle/details";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("multi", "uids", uids));

    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseIndexVehicleDetailResponseList> localVarReturnType = new GenericType<ClientResponseIndexVehicleDetailResponseList>() {};

    return apiClient.invokeAPI("VehicleApi.getVehicleDetails", localVarPath, "GET", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
}
