

# Change

All enhancements to the vehicle data that were made during data import.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rule** | **String** |  |  [optional]
**key** | **String** |  |  [optional]
**message** | **String** |  |  [optional]
**before** | **Map&lt;String, String&gt;** |  |  [optional]
**after** | **Map&lt;String, String&gt;** |  |  [optional]



