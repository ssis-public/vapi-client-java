

# IndexDealerContact

Contact person at the dealer.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order** | **Integer** | Contact order of the person. |  [optional]
**title** | **String** | Title of the contact. |  [optional]
**name** | **String** | Name of the contact. |  [optional]
**email** | **String** | The email. |  [optional]
**phone** | **String** | The phone number. |  [optional]
**fax** | **String** | The fax number. |  [optional]
**mobile** | **String** | The mobile number. |  [optional]
**imageUrl** | **String** | An image url of a photo of the contact. |  [optional]



