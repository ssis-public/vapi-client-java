# DealerApi

All URIs are relative to *https://vapi.ssis.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLoggedInDealerDetails**](DealerApi.md#getLoggedInDealerDetails) | **GET** /dealer/info | DealerApi.info()



## getLoggedInDealerDetails

> ClientResponseIndexDealerDetailResponseList getLoggedInDealerDetails()

DealerApi.info()

Get details for logged in dealer.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.DealerApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        DealerApi apiInstance = new DealerApi(defaultClient);
        try {
            ClientResponseIndexDealerDetailResponseList result = apiInstance.getLoggedInDealerDetails();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling DealerApi#getLoggedInDealerDetails");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**ClientResponseIndexDealerDetailResponseList**](ClientResponseIndexDealerDetailResponseList.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json;charset=utf-8, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |

