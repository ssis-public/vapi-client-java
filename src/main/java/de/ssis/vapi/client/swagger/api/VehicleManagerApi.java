package de.ssis.vapi.client.swagger.api;

import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiResponse;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.Pair;

import javax.ws.rs.core.GenericType;

import java.math.BigDecimal;
import de.ssis.vapi.client.swagger.model.ClientResponseCommonResponse;
import de.ssis.vapi.client.swagger.model.ClientResponseManagerVehicle;
import de.ssis.vapi.client.swagger.model.ClientResponseObject;
import de.ssis.vapi.client.swagger.model.ClientResponseVehicleSaveResponse;
import de.ssis.vapi.client.swagger.model.ClientResponseVehicleUidList;
import de.ssis.vapi.client.swagger.model.DeleteVehiclesByUidOffernumberExceptRequest;
import de.ssis.vapi.client.swagger.model.ErrorDetails;
import de.ssis.vapi.client.swagger.model.ManagerVehicle;
import de.ssis.vapi.client.swagger.model.ManagerVehicleFilterParameters;
import java.time.OffsetDateTime;
import de.ssis.vapi.client.swagger.model.PaginationParameters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class VehicleManagerApi {
  private ApiClient apiClient;

  public VehicleManagerApi() {
    this(Configuration.getDefaultApiClient());
  }

  public VehicleManagerApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Get the API cilent
   *
   * @return API client
   */
  public ApiClient getApiClient() {
    return apiClient;
  }

  /**
   * Set the API cilent
   *
   * @param apiClient an instance of API client
   */
  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * delete all Vehicles
   * delete all Vehicles
   * @return ClientResponseCommonResponse
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseCommonResponse deleteAllVehicles() throws ApiException {
    return deleteAllVehiclesWithHttpInfo().getData();
  }

  /**
   * delete all Vehicles
   * delete all Vehicles
   * @return ApiResponse&lt;ClientResponseCommonResponse&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseCommonResponse> deleteAllVehiclesWithHttpInfo() throws ApiException {
    Object localVarPostBody = null;
    
    // create path and map variables
    String localVarPath = "/manage/vehicle/all";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseCommonResponse> localVarReturnType = new GenericType<ClientResponseCommonResponse>() {};

    return apiClient.invokeAPI("VehicleManagerApi.deleteAllVehicles", localVarPath, "DELETE", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * delete all Vehicles except the ones with the given uids or offernumbers
   * delete all Vehicles except
   * @param deleteVehiclesByUidOffernumberExceptRequest  (required)
   * @return ClientResponseObject
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseObject deleteAllVehiclesExcept(DeleteVehiclesByUidOffernumberExceptRequest deleteVehiclesByUidOffernumberExceptRequest) throws ApiException {
    return deleteAllVehiclesExceptWithHttpInfo(deleteVehiclesByUidOffernumberExceptRequest).getData();
  }

  /**
   * delete all Vehicles except the ones with the given uids or offernumbers
   * delete all Vehicles except
   * @param deleteVehiclesByUidOffernumberExceptRequest  (required)
   * @return ApiResponse&lt;ClientResponseObject&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseObject> deleteAllVehiclesExceptWithHttpInfo(DeleteVehiclesByUidOffernumberExceptRequest deleteVehiclesByUidOffernumberExceptRequest) throws ApiException {
    Object localVarPostBody = deleteVehiclesByUidOffernumberExceptRequest;
    
    // verify the required parameter 'deleteVehiclesByUidOffernumberExceptRequest' is set
    if (deleteVehiclesByUidOffernumberExceptRequest == null) {
      throw new ApiException(400, "Missing the required parameter 'deleteVehiclesByUidOffernumberExceptRequest' when calling deleteAllVehiclesExcept");
    }
    
    // create path and map variables
    String localVarPath = "/manage/vehicle/delete/except";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseObject> localVarReturnType = new GenericType<ClientResponseObject>() {};

    return apiClient.invokeAPI("VehicleManagerApi.deleteAllVehiclesExcept", localVarPath, "POST", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * delete the Vehicle with the given offerNumber
   * delete the Vehicle with the given offerNumber
   * @param offerNumber  (required)
   * @return ClientResponseCommonResponse
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseCommonResponse deleteVehicleByOfferNumber(String offerNumber) throws ApiException {
    return deleteVehicleByOfferNumberWithHttpInfo(offerNumber).getData();
  }

  /**
   * delete the Vehicle with the given offerNumber
   * delete the Vehicle with the given offerNumber
   * @param offerNumber  (required)
   * @return ApiResponse&lt;ClientResponseCommonResponse&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseCommonResponse> deleteVehicleByOfferNumberWithHttpInfo(String offerNumber) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'offerNumber' is set
    if (offerNumber == null) {
      throw new ApiException(400, "Missing the required parameter 'offerNumber' when calling deleteVehicleByOfferNumber");
    }
    
    // create path and map variables
    String localVarPath = "/manage/vehicle"
      .replaceAll("\\{" + "offerNumber" + "\\}", apiClient.escapeString(offerNumber.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseCommonResponse> localVarReturnType = new GenericType<ClientResponseCommonResponse>() {};

    return apiClient.invokeAPI("VehicleManagerApi.deleteVehicleByOfferNumber", localVarPath, "DELETE", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * delete the Vehicle with the given vehicleUid
   * delete the Vehicle with the given vehicleUid
   * @param uid  (required)
   * @return ClientResponseCommonResponse
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseCommonResponse deleteVehicleByUid(String uid) throws ApiException {
    return deleteVehicleByUidWithHttpInfo(uid).getData();
  }

  /**
   * delete the Vehicle with the given vehicleUid
   * delete the Vehicle with the given vehicleUid
   * @param uid  (required)
   * @return ApiResponse&lt;ClientResponseCommonResponse&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseCommonResponse> deleteVehicleByUidWithHttpInfo(String uid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'uid' is set
    if (uid == null) {
      throw new ApiException(400, "Missing the required parameter 'uid' when calling deleteVehicleByUid");
    }
    
    // create path and map variables
    String localVarPath = "/manage/vehicle/{uid}"
      .replaceAll("\\{" + "uid" + "\\}", apiClient.escapeString(uid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseCommonResponse> localVarReturnType = new GenericType<ClientResponseCommonResponse>() {};

    return apiClient.invokeAPI("VehicleManagerApi.deleteVehicleByUid", localVarPath, "DELETE", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * get the Vehicle with the given vehicleUid
   * get the Vehicle with the given vehicle
   * @param uid  (required)
   * @return ClientResponseManagerVehicle
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseManagerVehicle getVehicle(String uid) throws ApiException {
    return getVehicleWithHttpInfo(uid).getData();
  }

  /**
   * get the Vehicle with the given vehicleUid
   * get the Vehicle with the given vehicle
   * @param uid  (required)
   * @return ApiResponse&lt;ClientResponseManagerVehicle&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseManagerVehicle> getVehicleWithHttpInfo(String uid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'uid' is set
    if (uid == null) {
      throw new ApiException(400, "Missing the required parameter 'uid' when calling getVehicle");
    }
    
    // create path and map variables
    String localVarPath = "/manage/vehicle/{uid}"
      .replaceAll("\\{" + "uid" + "\\}", apiClient.escapeString(uid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseManagerVehicle> localVarReturnType = new GenericType<ClientResponseManagerVehicle>() {};

    return apiClient.invokeAPI("VehicleManagerApi.getVehicle", localVarPath, "GET", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * get a list of all vehicleUids that belong to the current Dealer
   * get a list of all vehicleUids that belong to the current Dealer
   * @param filter  (required)
   * @param pagination  (required)
   * @return ClientResponseVehicleUidList
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseVehicleUidList getVehicleUids(ManagerVehicleFilterParameters filter, PaginationParameters pagination) throws ApiException {
    return getVehicleUidsWithHttpInfo(filter, pagination).getData();
  }

  /**
   * get a list of all vehicleUids that belong to the current Dealer
   * get a list of all vehicleUids that belong to the current Dealer
   * @param filter  (required)
   * @param pagination  (required)
   * @return ApiResponse&lt;ClientResponseVehicleUidList&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseVehicleUidList> getVehicleUidsWithHttpInfo(ManagerVehicleFilterParameters filter, PaginationParameters pagination) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'filter' is set
    if (filter == null) {
      throw new ApiException(400, "Missing the required parameter 'filter' when calling getVehicleUids");
    }
    
    // verify the required parameter 'pagination' is set
    if (pagination == null) {
      throw new ApiException(400, "Missing the required parameter 'pagination' when calling getVehicleUids");
    }
    
    // create path and map variables
    String localVarPath = "/manage/vehicle";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "filter", filter));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "pagination", pagination));

    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseVehicleUidList> localVarReturnType = new GenericType<ClientResponseVehicleUidList>() {};

    return apiClient.invokeAPI("VehicleManagerApi.getVehicleUids", localVarPath, "GET", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * get a list of all vehicleUids that belong to the current Dealer
   * get a list of all vehicleUids that belong to the current Dealer
   * @param offerNumber  (optional)
   * @param changedBefore  (optional)
   * @param changedAfter  (optional)
   * @param sendBefore  (optional)
   * @param sendAfter  (optional)
   * @param hasErrors  (optional)
   * @param makeKey  (optional)
   * @param modelKey  (optional)
   * @param minDealerPrice  (optional)
   * @param maxDealerPrice  (optional)
   * @param minConsumerPrice  (optional)
   * @param maxConsumerPrice  (optional)
   * @param resultCount  (optional)
   * @param resultOffset  (optional)
   * @param sortType  (optional)
   * @param sortOrderASC  (optional)
   * @return ClientResponseVehicleUidList
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseVehicleUidList getVehicleUidsWithFlatParameters(String offerNumber, OffsetDateTime changedBefore, OffsetDateTime changedAfter, OffsetDateTime sendBefore, OffsetDateTime sendAfter, Boolean hasErrors, String makeKey, String modelKey, BigDecimal minDealerPrice, BigDecimal maxDealerPrice, BigDecimal minConsumerPrice, BigDecimal maxConsumerPrice, Integer resultCount, Integer resultOffset, String sortType, Boolean sortOrderASC) throws ApiException {
    return getVehicleUidsWithFlatParametersWithHttpInfo(offerNumber, changedBefore, changedAfter, sendBefore, sendAfter, hasErrors, makeKey, modelKey, minDealerPrice, maxDealerPrice, minConsumerPrice, maxConsumerPrice, resultCount, resultOffset, sortType, sortOrderASC).getData();
  }

  /**
   * get a list of all vehicleUids that belong to the current Dealer
   * get a list of all vehicleUids that belong to the current Dealer
   * @param offerNumber  (optional)
   * @param changedBefore  (optional)
   * @param changedAfter  (optional)
   * @param sendBefore  (optional)
   * @param sendAfter  (optional)
   * @param hasErrors  (optional)
   * @param makeKey  (optional)
   * @param modelKey  (optional)
   * @param minDealerPrice  (optional)
   * @param maxDealerPrice  (optional)
   * @param minConsumerPrice  (optional)
   * @param maxConsumerPrice  (optional)
   * @param resultCount  (optional)
   * @param resultOffset  (optional)
   * @param sortType  (optional)
   * @param sortOrderASC  (optional)
   * @return ApiResponse&lt;ClientResponseVehicleUidList&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseVehicleUidList> getVehicleUidsWithFlatParametersWithHttpInfo(String offerNumber, OffsetDateTime changedBefore, OffsetDateTime changedAfter, OffsetDateTime sendBefore, OffsetDateTime sendAfter, Boolean hasErrors, String makeKey, String modelKey, BigDecimal minDealerPrice, BigDecimal maxDealerPrice, BigDecimal minConsumerPrice, BigDecimal maxConsumerPrice, Integer resultCount, Integer resultOffset, String sortType, Boolean sortOrderASC) throws ApiException {
    Object localVarPostBody = null;
    
    // create path and map variables
    String localVarPath = "/manage/vehicle/flat_parameters";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "offerNumber", offerNumber));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "changedBefore", changedBefore));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "changedAfter", changedAfter));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sendBefore", sendBefore));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sendAfter", sendAfter));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "hasErrors", hasErrors));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "makeKey", makeKey));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "modelKey", modelKey));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "minDealerPrice", minDealerPrice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "maxDealerPrice", maxDealerPrice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "minConsumerPrice", minConsumerPrice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "maxConsumerPrice", maxConsumerPrice));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "resultCount", resultCount));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "resultOffset", resultOffset));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sortType", sortType));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "sortOrderASC", sortOrderASC));

    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseVehicleUidList> localVarReturnType = new GenericType<ClientResponseVehicleUidList>() {};

    return apiClient.invokeAPI("VehicleManagerApi.getVehicleUidsWithFlatParameters", localVarPath, "GET", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * save a Vehicle
   * save a Vehicle
   * @param managerVehicle  (required)
   * @return ClientResponseVehicleSaveResponse
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseVehicleSaveResponse saveVehicle(ManagerVehicle managerVehicle) throws ApiException {
    return saveVehicleWithHttpInfo(managerVehicle).getData();
  }

  /**
   * save a Vehicle
   * save a Vehicle
   * @param managerVehicle  (required)
   * @return ApiResponse&lt;ClientResponseVehicleSaveResponse&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseVehicleSaveResponse> saveVehicleWithHttpInfo(ManagerVehicle managerVehicle) throws ApiException {
    Object localVarPostBody = managerVehicle;
    
    // verify the required parameter 'managerVehicle' is set
    if (managerVehicle == null) {
      throw new ApiException(400, "Missing the required parameter 'managerVehicle' when calling saveVehicle");
    }
    
    // create path and map variables
    String localVarPath = "/manage/vehicle";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json;charset=utf-8", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseVehicleSaveResponse> localVarReturnType = new GenericType<ClientResponseVehicleSaveResponse>() {};

    return apiClient.invokeAPI("VehicleManagerApi.saveVehicle", localVarPath, "POST", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
}
