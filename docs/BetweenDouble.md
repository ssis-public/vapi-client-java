

# BetweenDouble

This class is used to enter two values, to search a value is between.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **Double** | The begin value. |  [optional]
**until** | **Double** | The end value. |  [optional]



