

# IndexDealerProduct

Product of a dealer booked at SSiS.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | The unique key of the product. |  [optional]
**name** | **String** | The name of the product. |  [optional]
**description** | **String** | The description of the product. |  [optional]
**expire** | **OffsetDateTime** | The expiring date of the product. |  [optional]
**status** | [**IndexDealerProductStatus**](IndexDealerProductStatus.md) |  |  [optional]
**options** | [**List&lt;IndexDealerProductOption&gt;**](IndexDealerProductOption.md) | Further options related to the product. |  [optional]



