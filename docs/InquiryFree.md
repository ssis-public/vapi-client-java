

# InquiryFree

The free inquiry data.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**makeKey** | **String** | The key of a make. |  [optional]
**modelKey** | **String** | The key of a model. |  [optional]
**versionText** | **String** | A further specification of the model. |  [optional]
**deliverycategoryKey** | **List&lt;String&gt;** | The delivery category key. Valid values are in static value class Deliverycategory. |  [optional]
**firstRegistrationDate** | [**BetweenLocalDate**](BetweenLocalDate.md) |  |  [optional]
**salesPrice** | [**BetweenDouble**](BetweenDouble.md) |  |  [optional]
**fuelTypeKey** | **List&lt;String&gt;** | The fuel type key. Valid values are in static value class FuelType. |  [optional]
**consumptionCombined** | [**BetweenDouble**](BetweenDouble.md) |  |  [optional]
**transmissionTypeKey** | **List&lt;String&gt;** | The transmission type key of the vehicle. Valid values are in static value class Transmission. |  [optional]
**mileage** | [**BetweenInteger**](BetweenInteger.md) |  |  [optional]
**doorCount** | [**BetweenInteger**](BetweenInteger.md) |  |  [optional]
**enginePowerKw** | [**BetweenShort**](BetweenShort.md) |  |  [optional]
**exteriorColor** | [**List&lt;VehicleExteriorSearch&gt;**](VehicleExteriorSearch.md) | The exterior color and paint combination. |  [optional]
**featuresJson** | **String** | Only selected. Eg. {\&quot;Einparkhilfe - Selbstlenkende Systeme\&quot;: true} |  [optional]



