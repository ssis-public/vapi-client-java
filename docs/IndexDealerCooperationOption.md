

# IndexDealerCooperationOption

Option of a dealer cooperation.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | The unique key of the cooperation option. |  [optional]
**name** | **String** | The name of the cooperation option. |  [optional]
**value** | **String** | The value of the cooperation option. |  [optional]



