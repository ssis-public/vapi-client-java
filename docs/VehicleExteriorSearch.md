

# VehicleExteriorSearch

The exterior color and paint combination.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**colorKey** | **String** | The extrior color key. Valid values are in static value class Color. |  [optional]
**paintKey** | **String** | The extrior paint key. Valid values are in static value class Paint. |  [optional]



