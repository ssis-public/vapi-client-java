

# IndexDealerProductStatus

Status of a booked product.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | The unique key of the product status. |  [optional]
**name** | **String** | The name of the product status. |  [optional]



