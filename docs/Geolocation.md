

# Geolocation

The Elastic GeoPoint with latitude and longitude geo coordinate of the location.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **Double** | The latitude geo coordinate of the location. |  [optional]
**lon** | **Double** | The longitude geo coordinate of the location. |  [optional]



