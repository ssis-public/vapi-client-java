

# MetaData

Metadata for requests and responses.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | **String** | The time the request took. | 
**requestId** | **String** | The internal request ID. | 
**hostAddress** | **String** | The address of the host. | 
**requestTimestamp** | **OffsetDateTime** | The timestamp of the request. | 
**version** | **String** | The version number of the API. | 
**versionTimestamp** | **OffsetDateTime** | The API version timestamp. | 



