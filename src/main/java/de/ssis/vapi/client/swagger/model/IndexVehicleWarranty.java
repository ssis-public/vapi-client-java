/*
 * VAPI
 * <b>Build Time: 2022-12-09T17:30:18.023+01:00</b><br><p><a href=\"/public/changelog\" target=\"_blank\">Changelog</a></p><p class=\"clients\">API-Clients:<br><a href=\"https://gitlab.com/ssis-public/vapi-client-php\" target=\"_blank\">PHP-Client</a><br><a href=\"https://gitlab.com/ssis-public/vapi-client-java\" target=\"_blank\">Java-Client</a></p>
 *
 * The version of the OpenAPI document: 6.3.0-374
 * Contact: technik@ssis.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.vapi.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.ssis.vapi.client.swagger.handler.JSON;


/**
 * The warranty durations and prices.
 */
@ApiModel(description = "The warranty durations and prices.")
@JsonPropertyOrder({
  IndexVehicleWarranty.JSON_PROPERTY_WARRANTY_DURATION,
  IndexVehicleWarranty.JSON_PROPERTY_PRICE
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class IndexVehicleWarranty {
  public static final String JSON_PROPERTY_WARRANTY_DURATION = "warrantyDuration";
  private Integer warrantyDuration;

  public static final String JSON_PROPERTY_PRICE = "price";
  private Double price;


  public IndexVehicleWarranty warrantyDuration(Integer warrantyDuration) {
    this.warrantyDuration = warrantyDuration;
    return this;
  }

   /**
   * The duration of the warranty.
   * @return warrantyDuration
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "The duration of the warranty.")
  @JsonProperty(JSON_PROPERTY_WARRANTY_DURATION)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public Integer getWarrantyDuration() {
    return warrantyDuration;
  }


  @JsonProperty(JSON_PROPERTY_WARRANTY_DURATION)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setWarrantyDuration(Integer warrantyDuration) {
    this.warrantyDuration = warrantyDuration;
  }


  public IndexVehicleWarranty price(Double price) {
    this.price = price;
    return this;
  }

   /**
   * The price of the warranty.
   * @return price
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "The price of the warranty.")
  @JsonProperty(JSON_PROPERTY_PRICE)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public Double getPrice() {
    return price;
  }


  @JsonProperty(JSON_PROPERTY_PRICE)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setPrice(Double price) {
    this.price = price;
  }


  /**
   * Return true if this IndexVehicleWarranty object is equal to o.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IndexVehicleWarranty indexVehicleWarranty = (IndexVehicleWarranty) o;
    return Objects.equals(this.warrantyDuration, indexVehicleWarranty.warrantyDuration) &&
        Objects.equals(this.price, indexVehicleWarranty.price);
  }

  @Override
  public int hashCode() {
    return Objects.hash(warrantyDuration, price);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IndexVehicleWarranty {\n");
    sb.append("    warrantyDuration: ").append(toIndentedString(warrantyDuration)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

