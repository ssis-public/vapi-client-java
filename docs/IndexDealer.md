

# IndexDealer

The complete dealer data. When used as a search form, omit the unneeded fields.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** | The internal elastic id. | 
**company** | **String** | The company name of the dealer. | 
**companyAdditional** | **String** | Additional company information. |  [optional]
**street** | **String** | The street of the company address. |  [optional]
**zip** | **String** | The zipcode of the company address. |  [optional]
**city** | **String** | The city of the company address. |  [optional]
**countryKey** | **String** | The country of the company address. ISO 3166-1 alpha-2 format |  [optional]
**countryText** | **String** | The country of the company address. |  [optional]
**currencyIso** | **String** | The currency of the dealer. |  [optional]
**currencySymbol** | **String** | The currency symbol of the deaker. |  [optional]
**geoLocation** | [**Geolocation**](Geolocation.md) |  |  [optional]
**phone** | **String** | The phone number of the dealer. |  [optional]
**fax** | **String** | The fax number of the dealer. |  [optional]
**email** | **String** | The email address of the dealer. |  [optional]
**web** | **String** | The web address of the dealer. |  [optional]
**businessOrganisation** | **String** | The type of the business organisation. |  [optional]
**ceo** | **String** | The name of the CEO. |  [optional]
**vatin** | **String** | The VAT Id of the company. |  [optional]
**commercialRegisterNumber** | **String** | The commercial register number of the company. |  [optional]
**commercialRegisterCourt** | **String** | The commercial register court of the company. |  [optional]
**arbitrationOffice** | **String** | The arbitration office of the company. |  [optional]
**insuranceBrokerRegisterNumber** | **String** | The commercial register court of the company. |  [optional]
**bic** | **String** | The BIC of the companies bank account. |  [optional]
**iban** | **String** | The IBAN of the companies bank account. |  [optional]
**bank** | **String** | The name of the companies bank account. |  [optional]
**vat** | **Float** | The VAT used for dealer&#39;s vehicles. |  [optional]
**additionalInformation** | **String** | Additional information. |  [optional]
**products** | [**List&lt;IndexDealerProduct&gt;**](IndexDealerProduct.md) | Dealer products. |  [optional]
**contacts** | [**List&lt;IndexDealerContact&gt;**](IndexDealerContact.md) | Dealer contacts. |  [optional]
**cooperations** | [**List&lt;IndexDealerCooperation&gt;**](IndexDealerCooperation.md) | Dealer cooperations. |  [optional]
**settings** | [**List&lt;IndexDealerSetting&gt;**](IndexDealerSetting.md) | Dealer settings. |  [optional]
**dateIndexed** | **OffsetDateTime** | Dealer indexed timestamp. |  [optional]



