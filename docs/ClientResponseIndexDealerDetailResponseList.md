

# ClientResponseIndexDealerDetailResponseList

The general response wrapper.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metaData** | [**MetaData**](MetaData.md) |  | 
**response** | [**IndexDealerDetailResponseList**](IndexDealerDetailResponseList.md) |  | 



