package de.ssis.vapi.client.swagger.api;

import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiResponse;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.Pair;

import javax.ws.rs.core.GenericType;

import de.ssis.vapi.client.swagger.model.ClassListResponse;
import de.ssis.vapi.client.swagger.model.ErrorDetails;
import de.ssis.vapi.client.swagger.model.GeneralResponse;
import de.ssis.vapi.client.swagger.model.ListResponseClientResponseIntf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class StaticValuesApi {
  private ApiClient apiClient;

  public StaticValuesApi() {
    this(Configuration.getDefaultApiClient());
  }

  public StaticValuesApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Get the API cilent
   *
   * @return API client
   */
  public ApiClient getApiClient() {
    return apiClient;
  }

  /**
   * Set the API cilent
   *
   * @param apiClient an instance of API client
   */
  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Get all class names for static values.
   * Get all class names for static values.
   * @return ClassListResponse
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClassListResponse getAllStaticValueClasses() throws ApiException {
    return getAllStaticValueClassesWithHttpInfo().getData();
  }

  /**
   * Get all class names for static values.
   * Get all class names for static values.
   * @return ApiResponse&lt;ClassListResponse&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClassListResponse> getAllStaticValueClassesWithHttpInfo() throws ApiException {
    Object localVarPostBody = null;
    
    // create path and map variables
    String localVarPath = "/value";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClassListResponse> localVarReturnType = new GenericType<ClassListResponse>() {};

    return apiClient.invokeAPI("StaticValuesApi.getAllStaticValueClasses", localVarPath, "GET", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * Get all models of given make.
   * Get all models of given make.
   * @param make  (required)
   * @return ListResponseClientResponseIntf
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ListResponseClientResponseIntf getModelsByMake(String make) throws ApiException {
    return getModelsByMakeWithHttpInfo(make).getData();
  }

  /**
   * Get all models of given make.
   * Get all models of given make.
   * @param make  (required)
   * @return ApiResponse&lt;ListResponseClientResponseIntf&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ListResponseClientResponseIntf> getModelsByMakeWithHttpInfo(String make) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'make' is set
    if (make == null) {
      throw new ApiException(400, "Missing the required parameter 'make' when calling getModelsByMake");
    }
    
    // create path and map variables
    String localVarPath = "/value/models/{make}"
      .replaceAll("\\{" + "make" + "\\}", apiClient.escapeString(make.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ListResponseClientResponseIntf> localVarReturnType = new GenericType<ListResponseClientResponseIntf>() {};

    return apiClient.invokeAPI("StaticValuesApi.getModelsByMake", localVarPath, "GET", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * Get values of given static value class and key.
   * Get values of given static value class and key.
   * @param staticValueClassName  (required)
   * @param staticValueKey  (required)
   * @return GeneralResponse
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 404 </td><td> Not Found </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public GeneralResponse getStaticValueContentByKey(String staticValueClassName, String staticValueKey) throws ApiException {
    return getStaticValueContentByKeyWithHttpInfo(staticValueClassName, staticValueKey).getData();
  }

  /**
   * Get values of given static value class and key.
   * Get values of given static value class and key.
   * @param staticValueClassName  (required)
   * @param staticValueKey  (required)
   * @return ApiResponse&lt;GeneralResponse&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 404 </td><td> Not Found </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<GeneralResponse> getStaticValueContentByKeyWithHttpInfo(String staticValueClassName, String staticValueKey) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'staticValueClassName' is set
    if (staticValueClassName == null) {
      throw new ApiException(400, "Missing the required parameter 'staticValueClassName' when calling getStaticValueContentByKey");
    }
    
    // verify the required parameter 'staticValueKey' is set
    if (staticValueKey == null) {
      throw new ApiException(400, "Missing the required parameter 'staticValueKey' when calling getStaticValueContentByKey");
    }
    
    // create path and map variables
    String localVarPath = "/value/{staticValueClassName}/{staticValueKey}"
      .replaceAll("\\{" + "staticValueClassName" + "\\}", apiClient.escapeString(staticValueClassName.toString()))
      .replaceAll("\\{" + "staticValueKey" + "\\}", apiClient.escapeString(staticValueKey.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<GeneralResponse> localVarReturnType = new GenericType<GeneralResponse>() {};

    return apiClient.invokeAPI("StaticValuesApi.getStaticValueContentByKey", localVarPath, "GET", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
  /**
   * Get all values of given static value class.
   * Get all values of given static value class.
   * @param staticValueClassName  (required)
   * @return ListResponseClientResponseIntf
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 404 </td><td> Not Found </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ListResponseClientResponseIntf getStaticValueContents(String staticValueClassName) throws ApiException {
    return getStaticValueContentsWithHttpInfo(staticValueClassName).getData();
  }

  /**
   * Get all values of given static value class.
   * Get all values of given static value class.
   * @param staticValueClassName  (required)
   * @return ApiResponse&lt;ListResponseClientResponseIntf&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Success </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 404 </td><td> Not Found </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ListResponseClientResponseIntf> getStaticValueContentsWithHttpInfo(String staticValueClassName) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'staticValueClassName' is set
    if (staticValueClassName == null) {
      throw new ApiException(400, "Missing the required parameter 'staticValueClassName' when calling getStaticValueContents");
    }
    
    // create path and map variables
    String localVarPath = "/value/{staticValueClassName}"
      .replaceAll("\\{" + "staticValueClassName" + "\\}", apiClient.escapeString(staticValueClassName.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ListResponseClientResponseIntf> localVarReturnType = new GenericType<ListResponseClientResponseIntf>() {};

    return apiClient.invokeAPI("StaticValuesApi.getStaticValueContents", localVarPath, "GET", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
}
