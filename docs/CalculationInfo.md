

# CalculationInfo

The values used to calculate this vehicle.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distributorSelected** | **Boolean** |  |  [optional]
**priceGapSelected** | **Boolean** |  |  [optional]
**modelSelected** | **Boolean** |  |  [optional]
**vatStatusSelected** | **Boolean** |  |  [optional]
**pictureStatusSelected** | **Boolean** |  |  [optional]
**grossPurchasePrice** | **Double** |  |  [optional]
**distributorSurcharge** | **Double** |  |  [optional]
**priceGroupSurcharge** | **Double** |  |  [optional]
**warrantySurcharge** | **Double** |  |  [optional]
**rounding** | **Integer** |  |  [optional]
**reduction** | **Integer** |  |  [optional]
**featureSurcharge** | **Double** |  |  [optional]
**featureRounding** | **Integer** |  |  [optional]



