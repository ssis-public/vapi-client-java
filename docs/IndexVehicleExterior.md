

# IndexVehicleExterior

A vehicle can have multiple exteriors. One exterior has fields fields for: colorKey, paintKey, text, hexcode, price.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**colorKey** | **String** |  |  [optional]
**colorText** | **String** |  |  [optional]
**paintKey** | **String** |  |  [optional]
**paintText** | **String** |  |  [optional]
**text** | **String** |  |  [optional]
**hexCode** | **String** |  |  [optional]
**purchasePrice** | **Double** |  |  [optional]
**netSalesPrice** | **Double** |  |  [optional]
**grossSalesPrice** | **Double** |  |  [optional]
**salesPrice** | **Double** | The net sales price |  [optional] [readonly]



