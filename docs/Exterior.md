

# Exterior

A list of exterior choices.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**colorKey** | **String** |  |  [optional]
**paintKey** | **String** |  |  [optional]
**text** | **String** |  |  [optional]
**hexcode** | **String** |  |  [optional]
**price** | **BigDecimal** |  |  [optional]



