

# InquiryRequest

The request class for create new inquiry.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestorDealerId** | **Long** | Leave empty, if the request comes from end customer. |  [optional]
**requestedDealerId** | **Long** | Leave empty, if the request goes to all dealers. |  [optional]
**inquiryType** | **String** | The inquiry type. Possible values: INQUIRY, OFFER | 
**inquirySubtype** | **String** | The inquiry subtype. Possible values: INQUIRY_DIRECT_PUBLIC, INQUIRY_DIRECT_INTERN, INQUIRY_FREE_PUBLIC, INQUIRY_FREE_INTERN | 
**inquiryRequestor** | [**InquiryRequestor**](InquiryRequestor.md) |  | 
**inquiryVehicle** | [**InquiryVehicle**](InquiryVehicle.md) |  |  [optional]
**inquiryFree** | [**InquiryFree**](InquiryFree.md) |  |  [optional]
**timeOfPurchase** | **String** | When would the interested person like to buy the vehicle. |  [optional]
**customerMessage** | **String** | An additional message from the customer. |  [optional]



