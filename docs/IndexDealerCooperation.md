

# IndexDealerCooperation

Cooperations of a dealer registered at SSiS.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | The unique key of the cooperation. |  [optional]
**name** | **String** | The name of the cooperation. |  [optional]
**status** | [**IndexDealerCooperationStatus**](IndexDealerCooperationStatus.md) |  |  [optional]
**options** | [**List&lt;IndexDealerCooperationOption&gt;**](IndexDealerCooperationOption.md) | Further options related to the cooperation. |  [optional]



