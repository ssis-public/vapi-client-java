

# InquiryVehicle

Leave empty, if the request comes from end customer.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **String** |  |  [optional]
**featureUids** | **List&lt;String&gt;** |  |  [optional]



