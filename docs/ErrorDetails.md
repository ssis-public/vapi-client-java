

# ErrorDetails

General VAPI Error response class.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **OffsetDateTime** | The timestamp of the thrown error. | 
**message** | **String** | The error message. | 
**details** | **String** | Some error details. | 



