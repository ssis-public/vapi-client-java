package de.ssis.vapi.client.swagger.api;

import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiResponse;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.Pair;

import javax.ws.rs.core.GenericType;

import de.ssis.vapi.client.swagger.model.ClientResponseCommonResponse;
import de.ssis.vapi.client.swagger.model.ErrorDetails;
import de.ssis.vapi.client.swagger.model.InquiryRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class InquiriesApi {
  private ApiClient apiClient;

  public InquiriesApi() {
    this(Configuration.getDefaultApiClient());
  }

  public InquiriesApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Get the API cilent
   *
   * @return API client
   */
  public ApiClient getApiClient() {
    return apiClient;
  }

  /**
   * Set the API cilent
   *
   * @param apiClient an instance of API client
   */
  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Create an inquiry.
   * Create an inquiry.
   * @param inquiryRequest Create an inquiry. (required)
   * @return ClientResponseCommonResponse
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Updated </td><td>  -  </td></tr>
       <tr><td> 201 </td><td> Created </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ClientResponseCommonResponse create(InquiryRequest inquiryRequest) throws ApiException {
    return createWithHttpInfo(inquiryRequest).getData();
  }

  /**
   * Create an inquiry.
   * Create an inquiry.
   * @param inquiryRequest Create an inquiry. (required)
   * @return ApiResponse&lt;ClientResponseCommonResponse&gt;
   * @throws ApiException if fails to make API call
   * @http.response.details
     <table summary="Response Details" border="1">
       <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
       <tr><td> 200 </td><td> Updated </td><td>  -  </td></tr>
       <tr><td> 201 </td><td> Created </td><td>  -  </td></tr>
       <tr><td> 401 </td><td> Authorization required. Please use your token. </td><td>  -  </td></tr>
       <tr><td> 0 </td><td> Other error responses. </td><td>  -  </td></tr>
     </table>
   */
  public ApiResponse<ClientResponseCommonResponse> createWithHttpInfo(InquiryRequest inquiryRequest) throws ApiException {
    Object localVarPostBody = inquiryRequest;
    
    // verify the required parameter 'inquiryRequest' is set
    if (inquiryRequest == null) {
      throw new ApiException(400, "Missing the required parameter 'inquiryRequest' when calling create");
    }
    
    // create path and map variables
    String localVarPath = "/inquiry/create";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, String> localVarCookieParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/json"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "bearerAuth" };

    GenericType<ClientResponseCommonResponse> localVarReturnType = new GenericType<ClientResponseCommonResponse>() {};

    return apiClient.invokeAPI("InquiriesApi.create", localVarPath, "PUT", localVarQueryParams, localVarPostBody,
                               localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAccept, localVarContentType,
                               localVarAuthNames, localVarReturnType, false);
  }
}
