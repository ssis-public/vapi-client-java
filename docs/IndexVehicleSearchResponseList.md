

# IndexVehicleSearchResponseList

Response wrapper for vehicle search results.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hitsFound** | **Long** | Total hits found. | 
**hitsResponded** | **Integer** | Hits responded, depends on total hits and your pagination parameters. | 
**aggregations** | **Map&lt;String, Map&lt;String, Object&gt;&gt;** |  |  [optional]
**searchErrors** | **List&lt;String&gt;** | A list of search errors. |  [optional]
**searchHits** | [**List&lt;IndexVehicleSearchResponseItem&gt;**](IndexVehicleSearchResponseItem.md) |  |  [optional]



