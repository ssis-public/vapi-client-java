

# BetweenLocalDate

This class is used to enter two values, to search a value is between.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **LocalDate** | The begin value. |  [optional]
**until** | **LocalDate** | The end value. |  [optional]



