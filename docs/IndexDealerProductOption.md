

# IndexDealerProductOption

Option of a booked product.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | The unique key of the product option. |  [optional]
**name** | **String** | The name of the product option. |  [optional]
**value** | **String** | The value of the product option. |  [optional]



