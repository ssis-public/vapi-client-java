# StaticValuesApi

All URIs are relative to *https://vapi.ssis.de*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllStaticValueClasses**](StaticValuesApi.md#getAllStaticValueClasses) | **GET** /value | Get all class names for static values.
[**getModelsByMake**](StaticValuesApi.md#getModelsByMake) | **GET** /value/models/{make} | Get all models of given make.
[**getStaticValueContentByKey**](StaticValuesApi.md#getStaticValueContentByKey) | **GET** /value/{staticValueClassName}/{staticValueKey} | Get values of given static value class and key.
[**getStaticValueContents**](StaticValuesApi.md#getStaticValueContents) | **GET** /value/{staticValueClassName} | Get all values of given static value class.



## getAllStaticValueClasses

> ClassListResponse getAllStaticValueClasses()

Get all class names for static values.

Get all class names for static values.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.StaticValuesApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        StaticValuesApi apiInstance = new StaticValuesApi(defaultClient);
        try {
            ClassListResponse result = apiInstance.getAllStaticValueClasses();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling StaticValuesApi#getAllStaticValueClasses");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**ClassListResponse**](ClassListResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## getModelsByMake

> ListResponseClientResponseIntf getModelsByMake(make)

Get all models of given make.

Get all models of given make.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.StaticValuesApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        StaticValuesApi apiInstance = new StaticValuesApi(defaultClient);
        String make = "make_example"; // String | 
        try {
            ListResponseClientResponseIntf result = apiInstance.getModelsByMake(make);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling StaticValuesApi#getModelsByMake");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **make** | **String**|  |

### Return type

[**ListResponseClientResponseIntf**](ListResponseClientResponseIntf.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **0** | Other error responses. |  -  |


## getStaticValueContentByKey

> GeneralResponse getStaticValueContentByKey(staticValueClassName, staticValueKey)

Get values of given static value class and key.

Get values of given static value class and key.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.StaticValuesApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        StaticValuesApi apiInstance = new StaticValuesApi(defaultClient);
        String staticValueClassName = "staticValueClassName_example"; // String | 
        String staticValueKey = "staticValueKey_example"; // String | 
        try {
            GeneralResponse result = apiInstance.getStaticValueContentByKey(staticValueClassName, staticValueKey);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling StaticValuesApi#getStaticValueContentByKey");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **staticValueClassName** | **String**|  |
 **staticValueKey** | **String**|  |

### Return type

[**GeneralResponse**](GeneralResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **404** | Not Found |  -  |
| **0** | Other error responses. |  -  |


## getStaticValueContents

> ListResponseClientResponseIntf getStaticValueContents(staticValueClassName)

Get all values of given static value class.

Get all values of given static value class.

### Example

```java
// Import classes:
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.Configuration;
import de.ssis.vapi.client.swagger.handler.auth.*;
import de.ssis.vapi.client.swagger.handler.model.*;
import de.ssis.vapi.client.swagger.api.StaticValuesApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://vapi.ssis.de");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        StaticValuesApi apiInstance = new StaticValuesApi(defaultClient);
        String staticValueClassName = "staticValueClassName_example"; // String | 
        try {
            ListResponseClientResponseIntf result = apiInstance.getStaticValueContents(staticValueClassName);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling StaticValuesApi#getStaticValueContents");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **staticValueClassName** | **String**|  |

### Return type

[**ListResponseClientResponseIntf**](ListResponseClientResponseIntf.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Success |  -  |
| **401** | Authorization required. Please use your token. |  -  |
| **404** | Not Found |  -  |
| **0** | Other error responses. |  -  |

