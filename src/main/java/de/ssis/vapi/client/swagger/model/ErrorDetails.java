/*
 * VAPI
 * <b>Build Time: 2022-12-09T17:30:18.023+01:00</b><br><p><a href=\"/public/changelog\" target=\"_blank\">Changelog</a></p><p class=\"clients\">API-Clients:<br><a href=\"https://gitlab.com/ssis-public/vapi-client-php\" target=\"_blank\">PHP-Client</a><br><a href=\"https://gitlab.com/ssis-public/vapi-client-java\" target=\"_blank\">Java-Client</a></p>
 *
 * The version of the OpenAPI document: 6.3.0-374
 * Contact: technik@ssis.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package de.ssis.vapi.client.swagger.model;

import java.util.Objects;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.ssis.vapi.client.swagger.handler.JSON;


/**
 * General VAPI Error response class.
 */
@ApiModel(description = "General VAPI Error response class.")
@JsonPropertyOrder({
  ErrorDetails.JSON_PROPERTY_TIMESTAMP,
  ErrorDetails.JSON_PROPERTY_MESSAGE,
  ErrorDetails.JSON_PROPERTY_DETAILS
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class ErrorDetails {
  public static final String JSON_PROPERTY_TIMESTAMP = "timestamp";
  private OffsetDateTime timestamp;

  public static final String JSON_PROPERTY_MESSAGE = "message";
  private String message;

  public static final String JSON_PROPERTY_DETAILS = "details";
  private String details;


  public ErrorDetails timestamp(OffsetDateTime timestamp) {
    this.timestamp = timestamp;
    return this;
  }

   /**
   * The timestamp of the thrown error.
   * @return timestamp
  **/
  @ApiModelProperty(required = true, value = "The timestamp of the thrown error.")
  @JsonProperty(JSON_PROPERTY_TIMESTAMP)
  @JsonInclude(value = JsonInclude.Include.ALWAYS)

  public OffsetDateTime getTimestamp() {
    return timestamp;
  }


  @JsonProperty(JSON_PROPERTY_TIMESTAMP)
  @JsonInclude(value = JsonInclude.Include.ALWAYS)
  public void setTimestamp(OffsetDateTime timestamp) {
    this.timestamp = timestamp;
  }


  public ErrorDetails message(String message) {
    this.message = message;
    return this;
  }

   /**
   * The error message.
   * @return message
  **/
  @ApiModelProperty(required = true, value = "The error message.")
  @JsonProperty(JSON_PROPERTY_MESSAGE)
  @JsonInclude(value = JsonInclude.Include.ALWAYS)

  public String getMessage() {
    return message;
  }


  @JsonProperty(JSON_PROPERTY_MESSAGE)
  @JsonInclude(value = JsonInclude.Include.ALWAYS)
  public void setMessage(String message) {
    this.message = message;
  }


  public ErrorDetails details(String details) {
    this.details = details;
    return this;
  }

   /**
   * Some error details.
   * @return details
  **/
  @ApiModelProperty(required = true, value = "Some error details.")
  @JsonProperty(JSON_PROPERTY_DETAILS)
  @JsonInclude(value = JsonInclude.Include.ALWAYS)

  public String getDetails() {
    return details;
  }


  @JsonProperty(JSON_PROPERTY_DETAILS)
  @JsonInclude(value = JsonInclude.Include.ALWAYS)
  public void setDetails(String details) {
    this.details = details;
  }


  /**
   * Return true if this ErrorDetails object is equal to o.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErrorDetails errorDetails = (ErrorDetails) o;
    return Objects.equals(this.timestamp, errorDetails.timestamp) &&
        Objects.equals(this.message, errorDetails.message) &&
        Objects.equals(this.details, errorDetails.details);
  }

  @Override
  public int hashCode() {
    return Objects.hash(timestamp, message, details);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorDetails {\n");
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    details: ").append(toIndentedString(details)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

