

# IndexDealerCooperationStatus

Status of a dealer cooperation.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | The unique key of the cooperation status. |  [optional]
**name** | **String** | The name of the cooperation status. |  [optional]



