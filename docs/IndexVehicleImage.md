

# IndexVehicleImage

The vehicle images.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**small** | **String** |  |  [optional]
**medium** | **String** |  |  [optional]
**large** | **String** |  |  [optional]



