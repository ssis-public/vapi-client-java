

# IndexVehicleWarranty

The warranty durations and prices.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**warrantyDuration** | **Integer** | The duration of the warranty. |  [optional]
**price** | **Double** | The price of the warranty. |  [optional]



