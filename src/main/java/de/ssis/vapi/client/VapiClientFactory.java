package de.ssis.vapi.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.ssis.vapi.client.swagger.api.AuthenticationApi;
import de.ssis.vapi.client.swagger.handler.ApiClient;
import de.ssis.vapi.client.swagger.handler.ApiException;
import de.ssis.vapi.client.swagger.handler.auth.HttpBearerAuth;
import de.ssis.vapi.client.swagger.model.AuthenticationRequest;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 
 * @author der@ssis.de
 *
 */
public class VapiClientFactory {

	public final Long clockSkew = 60L;

	private Long minTokenLifetime = 3600L;

	private String username;
	private String password;
	private final ApiClient apiClient;

	@SuppressWarnings("rawtypes")
	final Map<Class, Object> apis = new HashMap<>();

	/**
	 * 
	 * @param config
	 * @param token
	 * @return
	 * @throws ApiException
	 */
	public static VapiClientFactory createFromConfig(final Properties config, final String token) throws ApiException {
		final ApiClient apiClient = new ApiClient();

		if (config.containsKey("basePath")) {
			apiClient.setBasePath(config.getProperty("basePath"));
		}
		if (config.containsKey("connectionTimeout")) {
			apiClient.setConnectTimeout(Integer.valueOf(config.getProperty("connectionTimeout")));
		}
		if (config.containsKey("debugging")) {
			apiClient.setDebugging(Boolean.valueOf(config.getProperty("debugging")));
		}
		if (config.containsKey("readTimeout")) {
			apiClient.setReadTimeout(Integer.valueOf(config.getProperty("readTimeout")));
		}
		if (config.containsKey("tempFolderPath")) {
			apiClient.setTempFolderPath(config.getProperty("tempFolderPath"));
		}
		if (config.containsKey("userAgent")) {
			apiClient.setUserAgent(config.getProperty("userAgent"));
		}

// disabled for use with jersey2, due to no functions available in jersey2 configured ApiClient to set. May be obsolete.

//		if (config.containsKey("verifyingSsl")) {
//			apiClient.setVerifyingSsl(Boolean.valueOf(config.getProperty("verifyingSsl")));
//		}
//		if (config.containsKey("writeTimeout")) {
//			apiClient.setWriteTimeout(Integer.valueOf(config.getProperty("writeTimeout")));
//		}
//
//		if (config.containsKey("sslCaCertPath")) {
//			InputStream sslCaCert;
//			try {
//				sslCaCert = new FileInputStream(config.getProperty("sslCaCertPath"));
//				apiClient.setSslCaCert(sslCaCert);
//			} catch (final FileNotFoundException e) {
//				throw new ApiException(e);
//			}
//		}

		return new VapiClientFactory(
			config.getProperty("username"),
			config.getProperty("password"),
			token,
			apiClient,
			config.containsKey("sslCaCertPath") ? Long.valueOf(config.getProperty("minTokenLifetime")) : null);
	}

	public VapiClientFactory(final String username, final String password) {
		this(username, password, null, null, null);
	}

	public VapiClientFactory(final String username, final String password, final String token) {
		this(username, password, token, null, null);
	}

	public VapiClientFactory(final String username, final String password, final String token, final Long minTokenLifetime) {
		this(username, password, token, null, null);
	}

	public VapiClientFactory(
			final String username,
			final String password,
			final String token,
			final ApiClient apiClient,
			final Long minTokenLifetime
	) {
		super();
		this.username  = username;
		this.password  = password;
		this.apiClient = null != apiClient ? apiClient : new ApiClient();
		((HttpBearerAuth) this.apiClient.getAuthentication("bearerAuth")).setBearerToken(token);
		if (null != minTokenLifetime) {
			this.minTokenLifetime = minTokenLifetime;
		}
	}

	public <T> Object getApi (final Class<T> clazz) throws ApiException {
		try {
			if (!apis.containsKey(clazz)) {
				apis.put(clazz, clazz.getDeclaredConstructor(ApiClient.class).newInstance(apiClient));
			}
			if (getRemainingTokenLifetime() < minTokenLifetime) {
				renewToken();
			}
			return apis.get(clazz);
		} catch (final Exception e) {
			throw new ApiException(e);
		}
	}

	public long getRemainingTokenLifetime () throws ApiException {
		final String token = getToken();
		if (null == token || token.isEmpty()) {
			return -1;
		}
		final String[] tokenParts = token.split("\\.");
		if (tokenParts.length != 3) {
			return -1;
		}
		@SuppressWarnings("unchecked")
		final Map<String, Object> tokenJsonObject;
		try {
			tokenJsonObject = new ObjectMapper().readValue(new String(Base64.getDecoder().decode(tokenParts[1])), Map.class);
		} catch (JsonProcessingException e) {
			throw new ApiException(e);
		}
		final Integer exp = (Integer) tokenJsonObject.get("exp");
		return null != exp ? Math.max(exp.longValue() - (long) Math.ceil(System.currentTimeMillis() / 1000L) - clockSkew, -1): -1;
	}

	public boolean tokenIsValid () throws ApiException {
		return getRemainingTokenLifetime() > 0;
	}

	public void renewToken () throws ApiException {
		final AuthenticationApi authApi = (AuthenticationApi) (apis.containsKey(AuthenticationApi.class) ? apis.get(AuthenticationApi.class) : new AuthenticationApi(apiClient));
		if (tokenIsValid()) {
			((HttpBearerAuth) apiClient.getAuthentication("bearerAuth")).setBearerToken(authApi.getNewToken().getToken());
		} else {
			final AuthenticationRequest authRequest = new AuthenticationRequest();
			authRequest.setName(username);
			authRequest.setPassword(password);
			((HttpBearerAuth) apiClient.getAuthentication("bearerAuth")).setBearerToken(authApi.login(authRequest).getToken());
		}
	}

	public String getUsername() {
		return username;
	}

	public VapiClientFactory setUsername(final String username) {
		this.username = username;
		return this;
	}

	public String getPassword() {
		return password;
	}

	public VapiClientFactory setPassword(final String password) {
		this.password = password;
		return this;
	}

	public String getToken() {
		return ((HttpBearerAuth) this.apiClient.getAuthentication("bearerAuth")).getBearerToken();
	}

	public VapiClientFactory setToken(final String token) {
		((HttpBearerAuth) this.apiClient.getAuthentication("bearerAuth")).setBearerToken(token);
		return this;
	}

	public ApiClient getApiClient() {
		return apiClient;
	}

	public Long getMinTokenLifetime() {
		return minTokenLifetime;
	}

	public VapiClientFactory setMinTokenLifetime(final long minTokenLifetime) {
		this.minTokenLifetime = minTokenLifetime;
		return this;
	}


}
