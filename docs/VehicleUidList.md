

# VehicleUidList

The object with the payload data.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicles** | [**List&lt;VehicleUidListItem&gt;**](VehicleUidListItem.md) |  |  [optional]
**count** | **Integer** |  |  [optional]



